-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 04 Sep 2019 pada 14.11
-- Versi Server: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `double`
--

DELIMITER $$
--
-- Fungsi
--
CREATE DEFINER=`root`@`localhost` FUNCTION `insert_admin`(`email` VARCHAR(50), `pass` VARCHAR(32), `nama` VARCHAR(100), `nip` VARCHAR(25), `id_toko` INT(11), `jenis_admin` ENUM('0','1')) RETURNS varchar(12) CHARSET latin1
BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(12);
  
  select count(*) into count_row_user from admin 
  	where SUBSTR(id_admin, 3,6) = left(NOW()+0, 6);
        
  select id_admin into last_key_user from admin
  	where SUBSTR(id_admin, 3,6) = left(NOW()+0, 6)
  	order by id_admin desc limit 1;
        
  if(count_row_user <1) then
  	set fix_key_user = concat("AD",left(NOW()+0, 6),"0001");
  else
    set fix_key_user = concat("AD",SUBSTR(last_key_user,3,10)+1);  
  END IF;
  
  insert into admin values(fix_key_user, email, pass, nama, nip, id_toko, jenis_admin, '0');
  
  return fix_key_user;
  
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id_admin` varchar(12) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(32) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `nip` varchar(25) NOT NULL,
  `id_toko` int(11) NOT NULL,
  `jenis_admin` enum('0','1') NOT NULL,
  `id_del` enum('0','1') NOT NULL,
  PRIMARY KEY (`id_admin`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id_admin`, `email`, `password`, `nama`, `nip`, `id_toko`, `jenis_admin`, `id_del`) VALUES
('AD2018100001', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Surya Hanggara', '1234567890', 1, '1', '0'),
('AD2018100002', 'admin2', '21232f297a57a5a743894a0e4a801fc3', 'Dimas', '123456789', 2, '1', '0'),
('AD2018100004', 'admin3', '21232f297a57a5a743894a0e4a801fc3', 'muhammad', '34242342', 1, '0', '0'),
('AD2018100005', 'admin4', 'as', 'admin', '12345', 2, '1', '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pendapatan`
--

CREATE TABLE IF NOT EXISTS `pendapatan` (
  `id_lap` int(10) NOT NULL AUTO_INCREMENT,
  `id_admin` varchar(12) NOT NULL,
  `tgl_input` datetime NOT NULL,
  `tgl` date NOT NULL,
  `periode` int(8) NOT NULL,
  `penjualan` varchar(50) NOT NULL,
  PRIMARY KEY (`id_lap`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=57 ;

--
-- Dumping data untuk tabel `pendapatan`
--

INSERT INTO `pendapatan` (`id_lap`, `id_admin`, `tgl_input`, `tgl`, `periode`, `penjualan`) VALUES
(1, 'AD2018100001', '2019-06-10 00:00:00', '2018-01-01', 2018011, '500'),
(2, 'AD2018100001', '2019-06-10 00:00:00', '2018-01-02', 2018012, '350'),
(3, 'AD2018100001', '2019-06-10 00:00:00', '2018-01-03', 2018013, '250'),
(4, 'AD2018100001', '2019-06-10 00:00:00', '2018-01-04', 2018014, '400'),
(5, 'AD2018100001', '2019-06-10 00:00:00', '2018-02-01', 2018021, '450'),
(6, 'AD2018100001', '2019-06-10 00:00:00', '2018-02-02', 2018022, '350'),
(7, 'AD2018100001', '2019-06-10 00:00:00', '2018-02-03', 2018023, '200'),
(8, 'AD2018100001', '2019-06-10 00:00:00', '2018-02-04', 2018024, '300'),
(9, 'AD2018100001', '2019-06-10 00:00:00', '2018-03-01', 2018031, '350'),
(10, 'AD2018100001', '2019-06-10 00:00:00', '2018-03-02', 2018032, '200'),
(11, 'AD2018100001', '2019-06-10 00:00:00', '2018-03-03', 2018033, '150'),
(12, 'AD2018100001', '2019-06-10 00:00:00', '2018-03-04', 2018034, '400'),
(13, 'AD2018100001', '2019-06-10 00:00:00', '2018-04-01', 2018041, '550'),
(14, 'AD2018100001', '2019-06-10 00:00:00', '2018-04-02', 2018042, '350'),
(15, 'AD2018100001', '2019-06-10 00:00:00', '2018-04-03', 2018043, '250'),
(16, 'AD2018100001', '2019-06-10 00:00:00', '2018-04-04', 2018044, '550'),
(17, 'AD2018100001', '2019-06-10 00:00:00', '2018-05-01', 2018051, '550'),
(18, 'AD2018100001', '2019-06-10 00:00:00', '2018-05-02', 2018052, '400'),
(19, 'AD2018100001', '2019-06-10 00:00:00', '2018-05-03', 2018053, '350'),
(20, 'AD2018100001', '2019-06-10 00:00:00', '2018-05-04', 2018054, '600'),
(21, 'AD2018100001', '2019-06-10 00:00:00', '2018-06-01', 2018061, '750'),
(22, 'AD2018100001', '2019-06-10 00:00:00', '2018-06-02', 2018062, '500'),
(23, 'AD2018100001', '2019-06-10 00:00:00', '2018-06-03', 2018063, '400'),
(24, 'AD2018100001', '2019-06-10 00:00:00', '2018-06-04', 2018064, '650'),
(25, 'AD2018100001', '2019-06-10 00:00:00', '2018-07-01', 2018071, '850'),
(26, 'AD2018100001', '2019-06-10 00:00:00', '2018-07-02', 2018072, '500'),
(27, 'AD2018100001', '2019-06-10 00:00:00', '2018-07-03', 2018073, '350'),
(28, 'AD2018100001', '2019-06-10 00:00:00', '2018-07-04', 2018074, '250'),
(29, 'AD2018100001', '2019-06-10 00:00:00', '2018-08-01', 2018081, '400'),
(30, 'AD2018100001', '2019-06-10 00:00:00', '2018-08-02', 2018082, '450'),
(31, 'AD2018100001', '2019-06-10 00:00:00', '2018-08-03', 2018083, '350'),
(32, 'AD2018100001', '2019-06-10 00:00:00', '2018-08-04', 2018084, '200'),
(33, 'AD2018100001', '2019-06-10 00:00:00', '2018-09-01', 2018091, '300'),
(34, 'AD2018100001', '2019-06-10 00:00:00', '2018-09-02', 2018092, '350'),
(35, 'AD2018100001', '2019-06-10 00:00:00', '2018-09-03', 2018093, '200'),
(36, 'AD2018100001', '2019-06-10 00:00:00', '2018-09-04', 2018094, '150'),
(37, 'AD2018100001', '2019-06-10 00:00:00', '2018-10-01', 2018101, '400'),
(38, 'AD2018100001', '2019-06-10 00:00:00', '2018-10-02', 2018102, '550'),
(39, 'AD2018100001', '2019-06-10 00:00:00', '2018-10-03', 2018103, '350'),
(40, 'AD2018100001', '2019-06-10 00:00:00', '2018-10-04', 2018104, '250'),
(41, 'AD2018100001', '2019-06-10 00:00:00', '2018-11-01', 2018111, '550'),
(42, 'AD2018100001', '2019-06-10 00:00:00', '2018-11-02', 2018112, '550'),
(43, 'AD2018100001', '2019-06-10 00:00:00', '2018-11-03', 2018113, '400'),
(44, 'AD2018100001', '2019-06-10 00:00:00', '2018-11-04', 2018114, '350'),
(45, 'AD2018100001', '2019-06-10 00:00:00', '2018-12-01', 2018121, '600'),
(46, 'AD2018100001', '2019-06-10 00:00:00', '2018-12-02', 2018122, '750'),
(47, 'AD2018100001', '2019-06-10 00:00:00', '2018-12-03', 2018123, '500'),
(48, 'AD2018100001', '2019-06-10 00:00:00', '2018-12-04', 2018124, '400'),
(49, 'AD2018100001', '2019-06-10 00:00:00', '2019-01-01', 2019011, '650'),
(50, 'AD2018100001', '2019-06-10 00:00:00', '2019-01-02', 2019012, '750');

-- --------------------------------------------------------

--
-- Struktur dari tabel `seting_alpha`
--

CREATE TABLE IF NOT EXISTS `seting_alpha` (
  `id_setting` int(11) NOT NULL AUTO_INCREMENT,
  `alpha` double NOT NULL,
  `beta` double NOT NULL,
  `sts_default` enum('0','1') NOT NULL,
  PRIMARY KEY (`id_setting`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `seting_alpha`
--

INSERT INTO `seting_alpha` (`id_setting`, `alpha`, `beta`, `sts_default`) VALUES
(1, 0.3, 0.1, '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `toko`
--

CREATE TABLE IF NOT EXISTS `toko` (
  `id_toko` int(11) NOT NULL AUTO_INCREMENT,
  `cabang` varchar(50) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  PRIMARY KEY (`id_toko`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data untuk tabel `toko`
--

INSERT INTO `toko` (`id_toko`, `cabang`, `alamat`) VALUES
(1, 'Kaliurang', 'JL Kaliurang No 42'),
(2, 'Teluk Bayur', 'JL Teluk Bayur 65'),
(5, 'Kalimosodo 2', 'JL Kalimososdo 2');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
