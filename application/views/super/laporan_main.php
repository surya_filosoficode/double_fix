                <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Data Penjualan</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    
                </div>
                <div>
                    <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">            
                <br>
                <!-- Row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-outline-info">
                            <!-- -----------------------------------------------------------------------------------------Data Filter-------------------------------------------------------------------------------------- -->
                            <div class="card-header">
                                <h4 class="m-b-0 text-white">Filter Data</h4>
                            </div>
                                <form method="post" action="<?=base_url()."user/mainreport/show_report";?>">
                                <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Periode</label>
                                                    <div class="col-md-9">
                                                        <select class="form-control" name="periode" id="periode">
                                                            <?php
                                                                $arr_month = array("Januari", "Februari", "Maret" ,"April", "Mei", "Juni", "Juli", "Agustus", "Septermber", "Oktober", "November", "Desember");
                                                                foreach ($arr_month as $r_arr_month => $v_arr_month) {
                                                                    echo "<option value=\"".($r_arr_month+1)."\">".$v_arr_month."</option>";
                                                                }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Tahun</label>
                                                    <div class="col-md-9">
                                                        <input type="number" class="form-control" placeholder="300000" id="th" name="th" value="<?= date("Y");?>" required="">
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->

                                            <div class="col-md-1">
                                                <div class="form-group row">
                                                    <div class="col-md-2">
                                                        <button type="button" class="btn btn-success" id="cek" style="vertical-align: bottom;">Cek Laporan</button>
                                                        <!-- <button type="button" class="btn btn-info"  style="vertical-align: bottom;">Cetak Laporan</button> -->
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="form-group row">
                                                    <div class="col-md-2">
                                                        <!-- <button type="button" class="btn btn-success"  style="vertical-align: bottom;">Cek Laporan</button> -->
                                                        <button type="submit" class="btn btn-info" id="cetak" style="vertical-align: bottom;">Cetak Laporan</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                                </form>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="card card-outline-inverse">
                            
                
                <!-- -----------------------------------------------------------------------------------------Data Table-------------------------------------------------------------------------------------- -->
                            <div class="card-header">
                                <h4 class="m-b-0 text-white">Data Table</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive m-t-40">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Tanggal</th>
                                                <th>Sales Net</th>
                                                <th>Struk</th>
                                                <th>Pergantian</th>
                                                <th>Variance</th>
                                                <th>Discount</th>
                                                <th>DSI Hari Ini</th>
                                                
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Tanggal</th>
                                                <th>Sales Net</th>
                                                <th>Struk</th>
                                                <th>Pergantian</th>
                                                <th>Variance</th>
                                                <th>Discount</th>
                                                <th>DSI Hari Ini</th>

                                            </tr>
                                        </tfoot>
                                        <tbody id="output_laporan">

                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- Row -->
            </div>
            <script type="text/javascript">
                $("#cek").click(function(){
                    var data_main =  new FormData();
                    // data_main.append('id_lap', id_lap);
                    var periode = 0;
                    var th = 0;
                    if($("#periode").val() != "0"){
                        periode = $("#periode").val();
                        // th = $("#th").val();
                    }

                    if( $("#th").val() != "0" &&  $("#th").val() != ""){
                        // periode = $("#periode").val();
                        th = $("#th").val();
                    }

                    data_main.append('periode',periode );
                    data_main.append('th', th);  

                    $.ajax({
                        url: "<?php echo base_url()."/user/mainreport/show_tbl/";?>", // point to server-side PHP script 
                        dataType: 'html',  // what to expect back from the PHP script, if anything
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: data_main,                         
                        type: 'post',
                        success: function(res){
                            // console.log(res);
                            show_search(res);
                        }
                    });

                    
                });

                function show_search(res){
                    $("#output_laporan").html(res);
                }
            </script>