                <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Laporan Pembelian Bahan Baku</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    
                </div>
                <div>
                    <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->


            <div class="container-fluid">            
                <br>
                <!-- Row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-outline-success">
                            <!-- -----------------------------------------------------------------------------------------Data Filter-------------------------------------------------------------------------------------- -->
                            <div class="card-header">
                                <h4 class="m-b-0 text-white">Filter Data</h4>
                            </div>
                                <form method="post" action="<?=base_url()."user/mainusersingle/show_report";?>" target="_blank">
                                <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-5">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Metode Filter</label>
                                                    <div class="demo-radio-button">
                                                        &nbsp;&nbsp;<input type="radio" id="radio_26" name="tipe_choose" value="0" />
                                                        <label for="radio_26">&nbsp;&nbsp;All</label>
                                                        <br>
                                                        &nbsp;&nbsp;<input type="radio" id="radio_27"  name="tipe_choose" value="1" checked=""/>
                                                        <label for="radio_27">&nbsp;&nbsp;Filter</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-7">
                                                <div class="form-group row">
                                                    <label class="control-label text-left col-md-3">Toko Cabang</label>
                                                    <div class="col-md-7">
                                                        <select class="form-control" name="cabang" id="cabang">
                                                            <?php
                                                            if(!empty($toko)){
                                                                foreach ($toko as $r_toko => $v_toko) {
                                                                    echo "<option value=\"".$v_toko->id_toko."\">".$v_toko->cabang." - ".$v_toko->alamat."</option>";
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-5">
                                                <div class="form-group row" id="in_periode">
                                                    <label class="control-label text-right col-md-3">Periode</label>
                                                    <div class="col-md-9">
                                                        <select class="form-control" name="periode" id="periode">
                                                            <!-- <option selected=""></option> -->
                                                            <?php
                                                                $arr_month = array("Januari", "Februari", "Maret" ,"April", "Mei", "Juni", "Juli", "Agustus", "Septermber", "Oktober", "November", "Desember");
                                                                foreach ($arr_month as $r_arr_month => $v_arr_month) {
                                                                    echo "<option value=\"".($r_arr_month+1)."\">".$v_arr_month."</option>";
                                                                }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-4">
                                                <div class="form-group row" id="in_th">
                                                    <label class="control-label text-right col-md-3">Tahun</label>
                                                    <div class="col-md-9">
                                                        <input type="number" class="form-control" placeholder="300000" id="th" name="th" value="<?= date("Y");?>" required="">
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->

                                            <div class="col-md-2 text-right">
                                                <div class="form-group row">
                                                    <div class="col-md-6">
                                                        <button type="button" class="btn btn-success" id="cek" style="vertical-align: bottom;">Filter</button>&nbsp;
                                                        <!-- <button type="submit" class="btn btn-success" id="cetak" style="vertical-align: bottom;">Cetak</button> -->
                                                        <!-- <button type="button" class="btn btn-success"  style="vertical-align: bottom;">Cetak Laporan</button> -->
                                                    </div>
                                                    <div class="col-md-6">
                                                        <!-- <button type="button" class="btn btn-success" id="cek" style="vertical-align: bottom;">Cek Laporan</button>&nbsp; -->
                                                        <button type="submit" class="btn btn-success" id="cetak" style="vertical-align: bottom;">Cetak Analisa</button>
                                                        <!-- <button type="button" class="btn btn-success"  style="vertical-align: bottom;">Cetak Laporan</button> -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                                </form>
                        </div>
                    </div>

                    <!-- <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Grafik Data Forecasting Pendapatan</h4>
                                <div id="chartdiv" style="width: 100%; height: 750px;"></div>
                            </div>
                        </div>
                    </div> -->

                    <div class="col-lg-12">
                        <div class="card card-outline-inverse">                
                            <!-------------------------------------------Data Table------------------------------------>
                            <div class="card-header">
                                <h4 class="m-b-0 text-white">Data Pembelian Bahan Baku</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive m-t-40">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th width="10%">No. </th>
                                                <th width="30%">Tanggal Pendapatan</th>
                                                <th width="30%">Nama Toko</th>
                                                <th width="40%">Total Pendapatan</th>
                                                
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            <?php
                                                if(!empty($penjualan)){
                                                    $sum_penjualan = 0;
                                                    foreach ($penjualan as $r_penjualan => $v_penjualan){
                                                        $sum_penjualan += $v_penjualan->penjualan;
                                                        echo "<tr>
                                                                <td align=\"center\">".($r_penjualan+1)."</td>
                                                                <td align=\"center\">".$v_penjualan->tgl."</td>
                                                                <td align=\"center\">".$v_penjualan->cabang."</td>
                                                                <td align=\"right\">Rp. ".number_format($v_penjualan->penjualan, 2,'.', ',')."</td>
                                                                
                                                            </tr>";
                                                    }
                                                }
                                            ?>

                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="3" align="center"><b>Jumlah</b></td>
                                                <td align="right"><b><?php print_r("Rp. ".number_format($sum_penjualan));?></b></td>
                                                
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>

                    
                </div>
                <!-- Row -->
            </div>

            <script type="text/javascript" src="<?= base_url()."assets/js/jquery-3.2.1.js";?>"></script>
            <!-- Resources -->
            <script src="<?= base_url()."assets/amchart4/";?>core.js"></script>
            <script src="<?= base_url()."assets/amchart4/";?>charts.js"></script>
            <script src="<?= base_url()."assets/amchart4/";?>animated.js"></script>

            <script type="text/javascript">
                // var tipe_choose;
                // var data_chart = JSON.parse('<?php //print_r($data_chart);?>');
                // console.log(data_chart);

                $(document).ready(function(){
                    $("input[name='tipe_choose']").change(function(){
                        tipe_choose = $("input[name='tipe_choose']:checked").val();
                        if(tipe_choose == 0) {
                            $("#in_periode").hide(200);
                            $("#in_th").hide(200);
                        }else{
                            $("#in_periode").show(200);
                            $("#in_th").show(200);
                        }
                        console.log();
                    });

                    // create_chart(data_chart);
                }); 

                // function create_chart(data_chart){
                //     am4core.ready(function() {

                //     // Themes begin
                //     am4core.useTheme(am4themes_animated);
                //     // Themes end

                //     // Create chart instance
                //     var chart = am4core.create("chartdiv", am4charts.XYChart);

                //     // Add data
                //     chart.data = data_chart;

                //     // Create category axis
                //     var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                //     categoryAxis.dataFields.category = "year";
                //     categoryAxis.renderer.opposite = true;

                //     // Create value axis
                //     var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                //     valueAxis.renderer.inversed = true;
                //     valueAxis.title.text = "Forecasting";
                //     valueAxis.renderer.minLabelPosition = 0.01;

                //     // Create series
                //     <?php //print_r($data_series);?>

                //     // Add chart cursor
                //     chart.cursor = new am4charts.XYCursor();
                //     chart.cursor.behavior = "zoomY";

                //     // Add legend
                //     chart.legend = new am4charts.Legend();

                //     }); // end am4core.ready()
                // }

                $("#cek").click(function(){
                    var tipe    = $("input[name='tipe_choose']:checked").val();
                    var periode = $("#periode").val();
                    var th      = $("#th").val();
                    var cabang  = $("#cabang").val();

                    var form = document.createElement("form");
                    form.setAttribute("method", "post");
                    form.setAttribute("action", "<?php echo base_url();?>super/laporan_pendapatan");
                    form.setAttribute("target", "_blank");

                    var hiddenField = document.createElement("input");
                    hiddenField.setAttribute("type", "hidden");
                    hiddenField.setAttribute("name", "tipe_choose");
                    hiddenField.setAttribute("value", tipe);

                    var hiddenField1 = document.createElement("input");
                    hiddenField1.setAttribute("type", "hidden");
                    hiddenField1.setAttribute("name", "periode");
                    hiddenField1.setAttribute("value", periode);

                    var hiddenField2 = document.createElement("input");
                    hiddenField2.setAttribute("type", "hidden");
                    hiddenField2.setAttribute("name", "th");
                    hiddenField2.setAttribute("value", th);

                    var hiddenField3 = document.createElement("input");
                    hiddenField3.setAttribute("type", "hidden");
                    hiddenField3.setAttribute("name", "cabang");
                    hiddenField3.setAttribute("value", cabang);

                    form.appendChild(hiddenField);
                    form.appendChild(hiddenField1);
                    form.appendChild(hiddenField2);
                    form.appendChild(hiddenField3);

                    document.body.appendChild(form);
                    form.submit();
                });

                $("#cetak").click(function(){
                    var tipe    = $("input[name='tipe_choose']:checked").val();
                    var periode = $("#periode").val();
                    var th      = $("#th").val();
                    var cabang  = $("#cabang").val();

                    var form = document.createElement("form");
                    form.setAttribute("method", "post");
                    form.setAttribute("action", "<?php echo base_url();?>super/Mainreportsuper/show_report");
                    form.setAttribute("target", "_blank");

                    var hiddenField = document.createElement("input");
                    hiddenField.setAttribute("type", "hidden");
                    hiddenField.setAttribute("name", "tipe_choose");
                    hiddenField.setAttribute("value", tipe);

                    var hiddenField1 = document.createElement("input");
                    hiddenField1.setAttribute("type", "hidden");
                    hiddenField1.setAttribute("name", "periode");
                    hiddenField1.setAttribute("value", periode);

                    var hiddenField2 = document.createElement("input");
                    hiddenField2.setAttribute("type", "hidden");
                    hiddenField2.setAttribute("name", "th");
                    hiddenField2.setAttribute("value", th);

                    var hiddenField3 = document.createElement("input");
                    hiddenField3.setAttribute("type", "hidden");
                    hiddenField3.setAttribute("name", "cabang");
                    hiddenField3.setAttribute("value", cabang);

                    form.appendChild(hiddenField);
                    form.appendChild(hiddenField1);
                    form.appendChild(hiddenField2);
                    form.appendChild(hiddenField3);

                    document.body.appendChild(form);
                    form.submit();
                });
            </script>