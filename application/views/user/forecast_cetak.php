                <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Data Single Exponensial Forecasting</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    
                </div>
                
            </div> -->

            <?php
                if(isset($data_alpha)){
                    if($data_alpha){
                        
                        $alpha  = $data_alpha["alpha"];
                        $beta   = $data_alpha["beta"];
                    }
                }

            ?>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">            
                <br>
                <!-- Row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-outline-inverse">                
                            <!-------------------------------------------Data Table------------------------------------>
                            <div class="card-header">

                                <h4 class="m-b-0 text-white">Data Perhitungan</h4>
                                <!-- Dengan Parameter Alpha = <?php print_r($alpha);?>, Beta = <?php print_r($beta);?> -->
                                <hr>
                                <br>
                                <!-- <h5 class="m-b-0 text-white">Parameter :</h5> -->

                                
                            </div>

                            <div class="card-body">
                                <div class="table-responsive m-t-40">
                                    <table id="example24" border="1" width="70%" style="border-collapse: collapse;">
                                        <thead>
                                            <tr>
                                                <th width="30%">Simbol</th>
                                                <th width="*">Keterangan</th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody id="output_laporan">
                                            <tr>
                                                <td>Alpha</td>
                                                <td><?php print_r($alpha);?></td>
                                            </tr>
                                            <tr>
                                                <td>Beta</td>
                                                <td><?php print_r($beta);?></td>
                                            </tr>
                                            <tr>
                                                <td>No (P)</td>
                                                <td>Periode Urutan Pembelian</td>
                                            </tr>
                                            <tr>
                                                <td>Periode (Minngu Ke)</td>
                                                <td>Pembelian Bahan Baku Pada Minggu-ke</td>
                                            </tr>
                                            <tr>
                                                <td>Tgl. Pembelian</td>
                                                <td>Tanggal Input Pembelian</td>
                                            </tr>
                                            <tr>
                                                <td>Total Pembelian</td>
                                                <td>Jumlah pembelian real</td>
                                            </tr>
                                            <tr>
                                                <td>lt</td>
                                                <td>(alpha x Total Pembelian) + ((1 - alpha) x (lt periode sebelumnya + tt periode sebelumnya))<br>
                                                ** Jika lt pada periode pertama maka nilai lt adalah sama dengan nilai pembelian pada periode pertama</td>
                                            </tr>
                                            <tr>
                                                <td>tt</td>
                                                <td>(beta x (lt - lt periode sebelumnya)) + ((1 - beta) x tt periode sebelumnya)<br>
                                                ** Jika tt pada periode pertama maka nilai tt adalah sama dengan 0</td>
                                            </tr>
                                            <tr>
                                                <td>Analisa Peramalan</td>
                                                <td>lt periode sebelumnya + (1 x tt periode sebelumnya)** Jika lt pada periode pertama maka nilai Analisa Peramalan adalah sama dengan nilai pembelian pada periode pertama</td>
                                            </tr>
                                            <tr>
                                                <td>Residual</td>
                                                <td>Nilai Pembelian Real - Nilai Analisa Peramalan</td>
                                            </tr>
                                            <tr>
                                                <td>MAD</td>
                                                <td>Nilai Residual yang DIPOSITIFKAN</td>
                                            </tr>
                                            <tr>
                                                <td>MSE</td>
                                                <td>Nilai Residual yang DI-KUADRATKAN</td>
                                            </tr>
                                            <tr>
                                                <td>MAPE</td>
                                                <td>MAD x 100 / pembelian real</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <br><br>
                                </div>

                            </div>


                            <div class="card-body">
                                <div class="table-responsive m-t-40">
                                    <table id="example24" border="1" width="100%" style="border-collapse: collapse;">
                                        <thead>
                                            <tr>
                                                <th width="5%">No (P)</th>
                                                <th width="15%">Periode (Minngu Ke)</th>
                                                <!-- <th width="10%">Tgl. Pembelian</th> -->
                                                <th width="10%">Total Pembelian</th>
                                                <th width="10%">lt</th>
                                                <th width="10%">tt</th>
                                                <th width="15%">Analisa Peramalan</th>
                                                <th width="5%">Residual</th>
                                                <th width="5%">MAD</th>
                                                <th width="5%">MSE</th>
                                                <th width="5%">MAPE</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>No</th>
                                                <th>Periode</th>
                                                <!-- <th>Tgl. Pembelian</th> -->
                                                <th>Total Pembelian</th>
                                                <th>lt</th>
                                                <th>tt</th>
                                                <th>Analisa Peramalan</th>
                                                <th>Residual</th>
                                                <th>MAD</th>
                                                <th>MSE</th>
                                                <th>MAPE</th>
                                            </tr>
                                        </tfoot>
                                        <tbody id="output_laporan">
                                            <?php
                                                $t_mad = 0;
                                                $t_mse = 0;
                                                $t_mape = 0;
                                                $n_data = 0;

                                                if($data_analisis){
                                                    $no = 1;
                                                    $n_data = count($data_analisis);
                                                    foreach ($data_analisis as $r_data_forecast => $v_data_forecast) {
                                                        // print_r("<pre>");
                                                        // print_r($v_data_forecast);
                                                        $str_periode = "minggu ke-".substr($v_data_forecast["periode"], 6, 1).", ".$month[(int)substr($v_data_forecast["periode"], 4, 2)]."/". substr($v_data_forecast["periode"], 0, 4);
                                                        print_r("<tr>
                                                                    <td align=\"center\">".$v_data_forecast["t"]."</td>
                                                                    <td align=\"right\">".$str_periode."</td>");
                                                                    // <td align=\"center\">".$v_data_forecast["tgl"]."</td>
                                                        print_r("   <td align=\"right\">".number_format($v_data_forecast["yt"], 2, ".", ",")." Gr.</td>
                                                                    <td align=\"right\">".number_format($v_data_forecast["lt"], 2, ".", ",")."</td>
                                                                    <td align=\"right\">".number_format($v_data_forecast["tt"], 2, ".", ",")."</td>
                                                                    <td align=\"right\">".number_format($v_data_forecast["yt_est"], 2, ".", ",")." Gr.</td>
                                                                    <td align=\"right\">".number_format($v_data_forecast["residual"], 2, ".", ",")."</td>
                                                                    <td align=\"right\">".number_format($v_data_forecast["mad"], 2, ".", ",")."</td>
                                                                    <td align=\"right\">".number_format($v_data_forecast["mse"], 2, ".", ",")."</td>
                                                                    <td align=\"right\">".number_format($v_data_forecast["mape"], 2, ".", ",")."</td>");
                                                        print_r("</tr>");

                                                        $t_mad += $v_data_forecast["mad"];
                                                        $t_mse += $v_data_forecast["mse"];
                                                        $t_mape += $v_data_forecast["mape"];

                                                        // print_r($count_array);
                                                        $no++;
                                                    }

                                                    // print_r($str_header);
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="card card-outline-inverse">                
                            <!-------------------------------------------Data Table------------------------------------>
                            <div class="card-header">
                                <br><br>
                                <h4 class="m-b-0 text-white">Data Forecasting</h4>
                                <hr><br>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive m-t-40">
                                    <table id="example23" border="1" width="50%" style="border-collapse: collapse;">
                                        <thead>
                                            <tr>
                                                <th width="5%">No</th>
                                                <th width="20%">Periode</th>
                                                <!-- <th width="10%">Tgl. Pembelian</th>
                                                <th width="10%">Total Pembelian</th>
                                                <th width="10%">lt</th>
                                                <th width="15%">tt</th> -->
                                                <th width="15%">Analisa Peramalan</th>
                                                <!-- <th width="15%">Residual</th> -->
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>No</th>
                                                <th>Periode</th>
                                                <!-- <th>Tgl. Pembelian</th>
                                                <th>Total Pembelian</th>
                                                <th>lt</th>
                                                <th>tt</th> -->
                                                <th>Analisa Peramalan</th>
                                                <!-- <th>Residual</th> -->
                                            </tr>
                                        </tfoot>
                                        <tbody id="output_laporan">
                                            <?php
                                                if($data_forecast){
                                                    $no = 1;
                                                    foreach ($data_forecast as $r_data_forecast => $v_data_forecast) {
                                                        // print_r("<pre>");
                                                        // print_r($v_data_forecast);
                                                         $str_periode = "minggu ke-".substr($v_data_forecast["periode"], 6, 1).", ".$month[(int)substr($v_data_forecast["periode"], 4, 2)]."/". substr($v_data_forecast["periode"], 0, 4);
                                                        print_r("<tr>
                                                                    <td align=\"center\">".$v_data_forecast["t"]."</td>
                                                                    <td align=\"right\">".$v_data_forecast["periode"]."</td>
                                                                    <td align=\"right\">".number_format($v_data_forecast["yt_est"], 2, ".", ",")." Gr.</td>");
                                                        print_r("</tr>");

                                                        // print_r($count_array);
                                                        $no++;
                                                    }

                                                    // print_r($str_header);
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="card card-outline-inverse">                
                            <!-------------------------------------------Data Table------------------------------------>
                            <div class="card-header">
                                <br><br>
                                <h4 class="m-b-0 text-white">Data Perhitungan Error</h4>
                                <hr><br>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive m-t-40">
                                    <table id="example23" border="1" width="50%" style="border-collapse: collapse;">
                                        <thead>
                                            <tr>
                                                <th width="5%">No</th>
                                                <th width="30%">Keterangan</th>
                                                <th width="*">Total</th>
                                                <th width="30%">Nilai Error</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th width="5%">No</th>
                                                <th width="30%">Keterangan</th>
                                                <th width="*">Total</th>
                                                <th width="30%">Nilai Error</th>
                                            </tr>
                                        </tfoot>
                                        <tbody id="output_laporan">
                                            <tr>
                                                <td align="center">1</td>
                                                <td align="center">Total Data</td>
                                                <td align="right"><?php print_r($n_data);?></td>
                                                <td align="right"><?php print_r("-");?></td>
                                            </tr>
                                            <tr>
                                                <td align="center">2</td>
                                                <td align="center">MAD</td>
                                                <td align="right"><?php print_r(number_format($t_mad, 2, ".", ",") ." / ". $n_data);?></td>
                                                <td align="right"><?php 
                                                                    $err_mad = 0;
                                                                    if($t_mad != 0){
                                                                        $err_mad = $t_mad/$n_data;
                                                                    }
                                                                    print_r(number_format($err_mad, 2, ".", ","));
                                                                ?></td>
                                            </tr>
                                            <tr>
                                                <td align="center">3</td>
                                                <td align="center">MSE</td>
                                                <td align="right"><?php print_r(number_format($t_mse, 2, ".", ",")." / ". $n_data);?></td>
                                                <td align="right"><?php
                                                                    $err_mse = 0;
                                                                    if($t_mse != 0){
                                                                        $err_mse = $t_mse/$n_data;
                                                                    }
                                                                    
                                                                    print_r(number_format($err_mse, 2, ".", ","));
                                                                ?></td>
                                            </tr>
                                            <tr>
                                                <td align="center">4</td>
                                                <td align="center">MAPE</td>
                                                <td align="right"><?php print_r(number_format($t_mape, 2, ".", ",")." / ". $n_data);?></td>
                                                <td align="right"><?php

                                                                    $err_mape = 0;
                                                                    if($t_mape != 0){
                                                                        $err_mape = $t_mape/$n_data;
                                                                    }

                                                                    
                                                                    print_r(number_format($err_mape, 2, ".", ","));
                                                                ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- Row -->
            </div>

            