<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainuser extends CI_Controller {

	public function __construct(){
		parent::__construct();	
		
		$this->load->model("user/main_user", "mu");
		$this->load->model("super/main_super", "ms");
		$this->load->library("response_message");

		if($this->session->userdata("double_log")["is_log"] != 1){
            redirect(base_url());
        }else{
        	if($this->session->userdata("double_log")["jenis_admin"] != 1){
                redirect(base_url());
            }
        }
        // print_r($this->session->userdata("double_log")["jenis_admin"]);
	}

	public function index(){
		$id_admin = $this->session->userdata("double_log")["id_admin"];

		// $id_setting = "1";

		$data_alpha = $this->mu->get_setting_default(array("sts_default"=>"1"));

		if(empty($data_alpha)){
			$data_alpha = $this->mu->get_alpha_first();
		}
		$alpha 	= (double)$data_alpha["alpha"];
		$beta 	= (double)$data_alpha["beta"];

		$p_forecast = 5;

		$array_analisis = array();

		$data_all = $this->mu->get_laporan_all($id_admin);

		// print_r($data_all);

		$no = 1;
		$key_before = 0;

		$no_json = 0;
		$array_json = array();

		foreach ($data_all as $key => $value) {
			if($no == 1){
				$array_analisis[$no] = array(
										"tgl"=>$value->tgl,
										"periode"=>$value->periode,
										"t"=>$no,
										"yt"=>$value->penjualan,
										"lt"=>$value->penjualan,
										"tt"=>0,
										"yt_est"=>$value->penjualan,
										"residual"=>0
									);

				$array_json[$no_json] = array(
											"periode"=>$no_json,
											"pembelian_real"=>$value->penjualan,
											"estimasi_pembelian"=>$value->penjualan
										);
			}else {
				$lt = $alpha*(double)$value->penjualan+(1-$alpha)*((double)$array_analisis[$no-1]["lt"] + (double)$array_analisis[$no-1]["tt"]);

				$tt = $beta * ($lt - (double)$array_analisis[$no-1]["lt"]) + (1 - $beta) * (double)$array_analisis[$no-1]["tt"];

				$yt_est = $array_analisis[$no-1]["yt"];
				if($array_analisis[$no-1]["tt"] != 0){
					$yt_est = (double)$array_analisis[$no-1]["lt"] + 1 * (double)$array_analisis[$no-1]["tt"];
				}

				$residual = (double)$value->penjualan - $yt_est;

				$array_analisis[$no] = array(
										"tgl"=>$value->tgl,
										"periode"=>$value->periode,
										"t"=>$no,
										"yt"=>$value->penjualan,
										"lt"=>$lt,

										"tt"=>$tt,
										"yt_est"=>$yt_est,
										"residual"=>$residual
									);

				$array_json[$no_json] = array(
											"periode"=>$no_json,
											"pembelian_real"=>$value->penjualan,
											"estimasi_pembelian"=>$yt_est
										);

			}

			$lt_end = $array_analisis[$no]["lt"];
			$tt_end = $array_analisis[$no]["tt"];
			$tgl_end= $value->tgl;
			$periode_end= $value->periode;

			$no++;

			$no_json++;			
		}

		$array_forecast = array();
		for ($i=1; $i <= $p_forecast ; $i++) { 
			$yt_est = (double)$lt_end + $i * (double)$tt_end;

			$periode_th = substr($periode_end, 6, 1);
			$periode_bln = substr($periode_end, 4, 2);
			$periode_mgg = substr($periode_end, 0, 4);

			if((int)$periode_mgg+$i >= 5){

			}

			$array_forecast[$i] = array(
										// "tgl"=>date('Y-m-d', strtotime('+'.$i.' days', strtotime($tgl_end))),
										"tgl"=>"0000-00-00",
										"periode"=>$i." - Periode yang akan datang",
										"t"=>$i,
										"yt"=>0,
										"lt"=>0,

										"tt"=>0,
										"yt_est"=>$yt_est,
										"residual"=>0
									);

			$array_json[$no_json] = array(
											"periode"=>$no_json,
											"pembelian_real"=>"0",
											"estimasi_pembelian"=>$yt_est
										);

			$no_json++;

		}

		// print_r("<pre>");
		// print_r($array_analisis);
		// print_r($array_forecast);
		// print_r($data_alpha);
		$data["page"] = "peramalan";
		$data["data_analisis"] = count($array_analisis);
		$data["data_forecast"] = $array_forecast;

		$data["data_json"] 	= json_encode($array_json);

		$data["month"] 		= array("","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");

		// print_r($data);

		// $this->load->view("index", $data);

		$data["page"] = "home";
		$this->load->view('index', $data);
	}

	public function index_alpha(){
		$data["page"] = "alpha";
		$data["alpha"] = $this->ms->get_all("seting_alpha");
		
		$this->load->view('index', $data);
	}

#========================================================================================================
#----------------------------------------- penjualan ----------------------------------------------------
#========================================================================================================
	public function index_oi(){
		$data["page"] = "penjualan";
		$this->load->view('index', $data);
	}

	public function index_penjualan(){
		$data["page"] = "penjualan";

		$id_admin = $this->session->userdata("double_log")["id_admin"];
		$data["penjualan"] = $this->mu->get_laporan_all_desc($id_admin);
		$data["month"] 		= array("","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");

		// print_r($data);
		$this->load->view('index', $data);
	}

	public function indexing_penjualan(){
		$periode = $this->input->post("periode");
		$th = $this->input->post("th");

		// print_r($_POST);

		$this->indexmain_penjualan($periode, $th);
	}

	public function indexmain_penjualan($periode, $th){
		$data["page"] = "penjualan";

		$id_admin = $this->session->userdata("double_log")["id_admin"];

		$data["penjualan"] = $this->mu->get_laporan_where($id_admin, (int)$periode, $th);

		$graph_data = array();
		$key_graph = 0;
		foreach ($data["penjualan"] as $r_data => $v_data) {
			$graph_data[$key_graph]["period"] = explode("-", $v_data->tgl)[2];
			$graph_data[$key_graph]["val_data"] = (int)$v_data->sales_net;
			$key_graph++;
		}
		$data["graph_data"] = json_encode($graph_data);

		$this->load->view('index', $data);
	}

	
	public function validaiton_form(){
		$config_val_input = array(
            array(
                'field'=>'tgl',
                'label'=>'Tanggal Input',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'penjualan',
                'label'=>'Penjualan',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'mgg_ke',
                'label'=>'mgg_ke',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'periode_bln',
                'label'=>'periode_bln',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'periode_th',
                'label'=>'periode_th',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_penjualan(){
		$main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
		$detail_msg = array(
			"tgl" => "",
			"sales_net" => ""
		);

		// print_r("<pre>");
		if($this->validaiton_form()){
			$tgl = $this->input->post("tgl");
			$penjualan = $this->input->post("penjualan");

			$mgg_ke = $this->input->post("mgg_ke");
			$periode_bln = $this->input->post("periode_bln");
			$periode_th = $this->input->post("periode_th");

			$periode = $periode_th.$periode_bln.$mgg_ke;

			$id_admin = $this->session->userdata("double_log")["id_admin"];
			$tgl_input = date("Y-m-d h:i:s");

			if(!$this->mu->check_periode(array("periode"=>$periode, "id_admin"=>$id_admin))){
				$data = array(
						"id_lap"=>"",
						"id_admin"=>$id_admin,
						"tgl_input"=>$tgl_input,
						"tgl"=>$tgl,
						"periode"=>$periode,
						"penjualan"=>$penjualan,
					);

				$insert = $this->mu->insert_laporan($data);
				if($insert){
					$main_msg = array("status" => true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
					$detail_msg = null;
				}else {
					$main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
					$detail_msg = null;
				}
			}else{
				$main_msg = array("status" => false, "msg"=>"insert data gagal, sudah terdapat data dengan periode ini");
			}

		}else {
			$main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
			$detail_msg = array(
					"tgl" => form_error("tgl"),
					"sales_net" => form_error("sales_net")
				);
		}

		$msg_array = $this->response_message->default_mgs($main_msg,$detail_msg);
		$this->session->set_flashdata("response_laporan", $msg_array);

		// print_r($msg_array);
		redirect(base_url()."admin/pendapatan");
	}

	public function delete_penjualan($id_penjualan){
		if($this->mu->delete_laporan(array("id_lap"=>$id_penjualan))){
			$main_msg = array("status" => true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
			$detail_msg = null;
		}else {
			$main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
			$detail_msg = null;
		}
		$msg_array = $this->response_message->default_mgs($main_msg,$detail_msg);
		print_r($msg_array);
		redirect(base_url()."admin/pendapatan");
	}

	public function index_up_penjualan(){
		$id_lap = $this->input->post("id_lap");
		$data["status"] = false;
		$data["val"] = null;
			
		if(!empty($this->mu->get_laporan_where_index_update($id_lap))){
			$data["status"] = true;
			$data["val"] = $this->mu->get_laporan_where_index_update($id_lap);
		}

		print_r(json_encode($data));
	}

	public function up_penjualan(){
		$main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
		$detail_msg = array(
			"tgl" => "",
			"sales_net" => ""
		);
		if($this->validaiton_form()){
			$id_lap = $this->input->post("id_lap");

			$mgg_ke = $this->input->post("mgg_ke");
			$periode_bln = $this->input->post("periode_bln");
			$periode_th = $this->input->post("periode_th");

			$periode = $periode_th.$periode_bln.$mgg_ke;

			$tgl = $this->input->post("tgl");
			$penjualan = $this->input->post("penjualan");

			$id_admin = $this->session->userdata("double_log")["id_admin"];
			$tgl_input = date("Y-m-d h:i:s");

			if(!$this->mu->check_periode(array("periode"=>$periode, "id_lap!="=>$id_lap, "id_admin"=>$id_admin))){
				$data = array(
						"tgl" 		=> $tgl,
						"penjualan" => $penjualan,
						"periode"	=> $periode,
						"id_admin"	=> $id_admin,
						"tgl_input" => $tgl_input
					);

				$where = array(
							"id_lap" => $id_lap
						);


				$update = $this->mu->update_laporan($data,$where);
				if($update){
					$main_msg = array("status" => true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
					$detail_msg = null;
				}else {
					$main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
					$detail_msg = null;
				}
			}else{
				$main_msg = array("status" => false, "msg"=>"insert data gagal, sudah terdapat data dengan periode ini");
			}
		}else {
			$main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
			$detail_msg = array(
					"tgl" => form_error("tgl"),
					"penjualan" => form_error("penjualan"),
				);
		}

		$msg_array = $this->response_message->default_mgs($main_msg,$detail_msg);
		$this->session->set_flashdata("response_laporan", $msg_array);

		// print_r($where);
		redirect(base_url()."admin/pendapatan");
	}
#========================================================================================================
#----------------------------------------- penjualan ----------------------------------------------------
#========================================================================================================

#========================================================================================================
#----------------------------------------- report_penjualan ---------------------------------------------
#========================================================================================================
	public function index_report_penjualan(){
		$data["page"] = "report_penjualan";
		$id_admin = $this->session->userdata("double_log")["id_admin"];
		$data_all = $this->mu->get_laporan_all($id_admin);

		if(isset($_POST["tipe_choose"])){
			$tipe_choose 	= $this->input->post("tipe_choose");
			$periode 		= $this->input->post("periode");
			$th 			= $this->input->post("th");

			if($tipe_choose == "1"){
				$data_all = $this->mu->get_laporan_where($id_admin, $periode, $th);	
			}
		}

		$data["penjualan"] = $data_all;
		$data["month"] 		= array("","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
		// print_r($_POST);

		$this->load->view('index', $data);
	}

	public function cetak_report_penjualan(){
		$data["page"] = "report_penjualan";
		$id_admin = $this->session->userdata("double_log")["id_admin"];
		$data_all = $this->mu->get_laporan_all($id_admin);

		if(isset($_POST["tipe_choose"])){
			$tipe_choose 	= $this->input->post("tipe_choose");
			$periode 		= $this->input->post("periode");
			$th 			= $this->input->post("th");

			if($tipe_choose == "1"){
				$data_all = $this->mu->get_laporan_where($id_admin, $periode, $th);	
			}
		}

		

		$data["penjualan"] = $data_all;
		$data["month"] 		= array("","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
		// print_r($data);

		$this->load->view('user/report_penjualan_cetak', $data);
	}
#========================================================================================================
#----------------------------------------- report_penjualan ---------------------------------------------
#========================================================================================================

#===========================================================================================
#--------------------------------------- Main alpha-----------------------------------------
#===========================================================================================
	private function validation_ins_alpha(){
		$config_val_input = array(
            array(
                'field'=>'alpha',
                'label'=>'alpha',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'beta',
                'label'=>'beta',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_alpha(){
		if($this->validation_ins_alpha()){
			$alpha = $this->input->post("alpha");
			$beta = $this->input->post("beta");
			$sts_default = $this->input->post("default");

			$send_ins = array("id_setting"=>"",
							"alpha"=>$alpha,
							"beta"=>$beta,
							"sts_default"=>$sts_default);

			$insert = $this->ms->insert_data("seting_alpha" ,$send_ins);
			if($insert){
				echo "true";
			}else {
				echo "false";
			}
			
		}

		redirect(base_url()."admin/alpha_uji");
	}

	public function index_up_alpha(){
		$id_setting = $this->input->post("id_setting");
		$data["status"] = false;
		$data["val"] = null;
			
		if(!empty($this->ms->get_where("seting_alpha", array("id_setting"=>$id_setting)))){
			$data["status"] = true;
			$data["val"] = $this->ms->get_where("seting_alpha", array("id_setting"=>$id_setting));
		}

		print_r(json_encode($data));
	}

	public function up_alpha(){
		if($this->validation_ins_alpha()){
			$alpha = $this->input->post("alpha");
			$beta = $this->input->post("beta");
			$sts_default = $this->input->post("default");

			$id_setting = $this->input->post("id_setting");

			$where_ins = array(
							"id_setting"=>$id_setting);

			$send_ins = array(
							"alpha"=>$alpha,
							"beta"=>$beta,
							"sts_default"=>$sts_default);

			$update = $this->ms->update_data("seting_alpha", $send_ins, $where_ins);
			if($update){
				echo "true";
			}else {
				echo "false";
			}
			
		}

		redirect(base_url()."admin/alpha_uji");
	}

	public function delete_alpha($id_setting){
		$delete = $this->ms->delete_data("seting_alpha", array("id_setting"=>$id_setting));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."admin/alpha_uji");
	}
#===========================================================================================
#--------------------------------------- Main alpha-----------------------------------------
#===========================================================================================


	public function index_laporan(){
		$data["page"] = "laporan";
		$this->load->view('index',$data);
	}

}
