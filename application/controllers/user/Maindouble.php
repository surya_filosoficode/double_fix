 <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Maindouble extends CI_Controller {

	public function __construct(){
		parent::__construct();	
		
		$this->load->model("user/main_user", "mu");
		$this->load->library("response_message");
		if($this->session->userdata("double_log")["is_log"] != 1){
            redirect(base_url());
        }else{
        	if($this->session->userdata("double_log")["jenis_admin"] != 1){
                redirect(base_url());
            }
        }
	}

	public function index(){
		$id_admin = $this->session->userdata("double_log")["id_admin"];

		// $id_setting = "1";

		$data_alpha = $this->mu->get_setting_default(array("sts_default"=>"1"));

		if(empty($data_alpha)){
			$data_alpha = $this->mu->get_alpha_first();
		}
		$alpha 	= (double)$data_alpha["alpha"];
		$beta 	= (double)$data_alpha["beta"];

		$p_forecast = 5;

		$array_analisis = array();

		$data_all = $this->mu->get_laporan_all($id_admin);

		// print_r($data_all);

		$no = 1;
		$key_before = 0;
		foreach ($data_all as $key => $value) {
			if($no == 1){
				$array_analisis[$no] = array(
										"tgl"=>$value->tgl,
										"periode"=>$value->periode,
										"t"=>$no,
										"yt"=>$value->penjualan,
										"lt"=>$value->penjualan,
										"tt"=>0,
										"yt_est"=>$value->penjualan,
										"residual"=>0
									);
			}else {
				$lt = $alpha*(double)$value->penjualan+(1-$alpha)*((double)$array_analisis[$no-1]["lt"] + (double)$array_analisis[$no-1]["tt"]);

				$tt = $beta * ($lt - (double)$array_analisis[$no-1]["lt"]) + (1 - $beta) * (double)$array_analisis[$no-1]["tt"];

				$yt_est = $array_analisis[$no-1]["yt"];
				if($array_analisis[$no-1]["tt"] != 0){
					$yt_est = (double)$array_analisis[$no-1]["lt"] + 1 * (double)$array_analisis[$no-1]["tt"];
				}

				$residual = (double)$value->penjualan - $yt_est;

				$array_analisis[$no] = array(
										"tgl"=>$value->tgl,
										"periode"=>$value->periode,
										"t"=>$no,
										"yt"=>$value->penjualan,
										"lt"=>$lt,

										"tt"=>$tt,
										"yt_est"=>$yt_est,
										"residual"=>$residual
									);

			}

			$lt_end = $array_analisis[$no]["lt"];
			$tt_end = $array_analisis[$no]["tt"];
			$tgl_end= $value->tgl;
			$periode_end= $value->periode;

			$no++;			
		}

		$array_forecast = array();
		for ($i=1; $i <= $p_forecast ; $i++) { 
			$yt_est = (double)$lt_end + $i * (double)$tt_end;

			$periode_th = substr($periode_end, 6, 1);
			$periode_bln = substr($periode_end, 4, 2);
			$periode_mgg = substr($periode_end, 0, 4);

			if((int)$periode_mgg+$i >= 5){

			}

			$array_forecast[$i] = array(
										// "tgl"=>date('Y-m-d', strtotime('+'.$i.' days', strtotime($tgl_end))),
										"tgl"=>"0000-00-00",
										"periode"=>$i." - Periode yang akan datang",
										"t"=>$i,
										"yt"=>0,
										"lt"=>0,

										"tt"=>0,
										"yt_est"=>$yt_est,
										"residual"=>0
									);

		}

		// print_r("<pre>");
		// print_r($array_analisis);
		// print_r($array_forecast);
		// print_r($data_alpha);
		$data["page"] = "peramalan";
		$data["data_analisis"] = $array_analisis;
		$data["data_forecast"] = $array_forecast;

		$data["month"] 		= array("","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");

		// print_r($data);

		$this->load->view("index", $data);
	}

#=========================================================================================
#---------------------------------------search_single-------------------------------------
#=========================================================================================
	public function index_search(){
		$tipe_choose 	= $this->input->post("tipe_choose");
		$periode 		= $this->input->post("periode");
		$th 			= $this->input->post("th");

		$id_admin = $this->session->userdata("double_log")["id_admin"];


		$data_alpha = $this->mu->get_setting_default(array("sts_default"=>"1"));

		if(empty($data_alpha)){
			$data_alpha = $this->mu->get_alpha_first();
		}
		$alpha 	= (double)$data_alpha["alpha"];
		$beta 	= (double)$data_alpha["beta"];

		$p_forecast = 5;

		$array_analisis = array();

		$data_all = $this->mu->get_laporan_all($id_admin);
		if($tipe_choose == "1"){
			$data_all = $this->mu->get_laporan_where($id_admin, $periode, $th);	
		}

		$no = 1;
		$key_before = 0;
		foreach ($data_all as $key => $value) {
			if($no == 1){
				$array_analisis[$no] = array(
										"tgl"=>$value->tgl,
										"periode"=>$value->periode,
										"t"=>$no,
										"yt"=>$value->penjualan,
										"lt"=>$value->penjualan,
										"tt"=>0,
										"yt_est"=>$value->penjualan,
										"residual"=>0
									);
			}else {
				$lt = $alpha*(double)$value->penjualan+(1-$alpha)*((double)$array_analisis[$no-1]["lt"] + (double)$array_analisis[$no-1]["tt"]);

				$tt = $beta * ($lt - (double)$array_analisis[$no-1]["lt"]) + (1 - $beta) * (double)$array_analisis[$no-1]["tt"];

				$yt_est = $array_analisis[$no-1]["yt"];
				if($array_analisis[$no-1]["tt"] != 0){
					$yt_est = (double)$array_analisis[$no-1]["lt"] + 1 * (double)$array_analisis[$no-1]["tt"];
				}

				$residual = (double)$value->penjualan - $yt_est;

				$array_analisis[$no] = array(
										"tgl"=>$value->tgl,
										"periode"=>$value->periode,
										"t"=>$no,
										"yt"=>$value->penjualan,
										"lt"=>$lt,

										"tt"=>$tt,
										"yt_est"=>$yt_est,
										"residual"=>$residual
									);

			}

			$lt_end = $array_analisis[$no]["lt"];
			$tt_end = $array_analisis[$no]["tt"];
			$tgl_end= $value->tgl;

			$no++;			
		}

		$array_forecast = array();
		if($array_analisis){
			for ($i=1; $i <= $p_forecast ; $i++) { 
			$yt_est = (double)$lt_end + $i * (double)$tt_end;

			$array_forecast[$i] = array(
										// "tgl"=>date('Y-m-d', strtotime('+'.$i.' days', strtotime($tgl_end))),
										"tgl"=>"0000-00-00",
										"periode"=>$i." - Periode yang akan datang",
										"t"=>$i,
										"yt"=>0,
										"lt"=>0,

										"tt"=>0,
										"yt_est"=>$yt_est,
										"residual"=>0
									);
			}
		}
		

		// print_r("<pre>");
		// print_r($_POST);
		// print_r($data_all);
		// print_r($array_analisis);
		// print_r($array_forecast);
		// print_r($data_alpha);
		$data["page"] = "peramalan";
		$data["data_analisis"] = $array_analisis;
		$data["data_forecast"] = $array_forecast;

		$data["month"] 		= array("","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");

		$this->load->view("index", $data);

		
	}
#=========================================================================================
#---------------------------------------search_single-------------------------------------
#=========================================================================================

#=========================================================================================
#---------------------------------------report_forecasting--------------------------------
#=========================================================================================

	public function show_report(){
		$tipe_choose 	= $this->input->post("tipe_choose");
		$periode 		= $this->input->post("periode");
		$th 			= $this->input->post("th");

		$id_admin = $this->session->userdata("double_log")["id_admin"];


		$data_alpha = $this->mu->get_setting_default(array("sts_default"=>"1"));
		// print_r($data_alpha);

		if(empty($data_alpha)){
			$data_alpha = $this->mu->get_alpha_first();
		}
		$alpha 	= (double)$data_alpha["alpha"];
		$beta 	= (double)$data_alpha["beta"];

		$p_forecast = 5;

		$array_analisis = array();

		$data_all = $this->mu->get_laporan_all($id_admin);
		if($tipe_choose == "1"){
			$data_all = $this->mu->get_laporan_where($id_admin, $periode, $th);	
		}

		$no = 1;
		$key_before = 0;
		foreach ($data_all as $key => $value) {
			if($no == 1){
				$mad_penjualan 	= $value->penjualan;
				$mad_est 		= $value->penjualan;

				$mad_count 		= sqrt(pow(((double)$mad_penjualan - (double)$mad_est), 2));
				$mse_count 		= pow(((double)$mad_penjualan - (double)$mad_est), 2);

				$mape_count		= (double)$mad_count * 100 / (double)$mad_penjualan;

				$array_analisis[$no] = array(
										"tgl"=>$value->tgl,
										"periode"=>$value->periode,
										"t"=>$no,
										"yt"=>$value->penjualan,
										"lt"=>$value->penjualan,
										"tt"=>0,
										"yt_est"=>$value->penjualan,
										"residual"=>0,
										"mad"=>$mad_count,
										"mse"=>$mse_count,
										"mape"=>$mape_count
									);

			}else {
				$lt = ($alpha*(double)$value->penjualan) + ((1-$alpha)*((double)$array_analisis[$no-1]["lt"] + (double)$array_analisis[$no-1]["tt"]));

				$tt = ($beta * ($lt - (double)$array_analisis[$no-1]["lt"])) + ((1 - $beta) * (double)$array_analisis[$no-1]["tt"]);

				$yt_est = $array_analisis[$no-1]["yt"];
				if($array_analisis[$no-1]["tt"] != 0){
					$yt_est = (double)$array_analisis[$no-1]["lt"] + (1 * (double)$array_analisis[$no-1]["tt"]);
				}

				$residual = (double)$value->penjualan - $yt_est;


				$mad_penjualan 	= $value->penjualan;
				$mad_est 		= $yt_est;

				$mad_count 		= sqrt(pow(((double)$mad_penjualan - (double)$mad_est), 2));
				$mse_count 		= pow(((double)$mad_penjualan - (double)$mad_est), 2);

				$mape_count		= (double)$mad_count * 100 / (double)$mad_penjualan;

				$array_analisis[$no] = array(
										"tgl"=>$value->tgl,
										"periode"=>$value->periode,
										"t"=>$no,
										"yt"=>$value->penjualan,
										"lt"=>$lt,

										"tt"=>$tt,
										"yt_est"=>$yt_est,
										"residual"=>$residual,
										"mad"=>$mad_count,
										"mse"=>$mse_count,
										"mape"=>$mape_count
									);
			}

			$lt_end = $array_analisis[$no]["lt"];
			$tt_end = $array_analisis[$no]["tt"];
			$tgl_end= $value->tgl;

			$no++;			
		}

		$array_forecast = array();
		if($array_analisis){
			for ($i=1; $i <= $p_forecast ; $i++) { 
			$yt_est = (double)$lt_end + $i * (double)$tt_end;

			$array_forecast[$i] = array(
										// "tgl"=>date('Y-m-d', strtotime('+'.$i.' days', strtotime($tgl_end))),
										"tgl"=>"0000-00-00",
										"periode"=>$i." - Periode yang akan datang",
										"t"=>$i,
										"yt"=>0,
										"lt"=>0,

										"tt"=>0,
										"yt_est"=>$yt_est,
										"residual"=>0
									);
			}
		}
		

		$data["page"] = "peramalan";

		$data["data_alpha"] = $data_alpha;

		$data["data_analisis"] = $array_analisis;
		$data["data_forecast"] = $array_forecast;
		$data["month"] 		= array("","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");	

		$this->load->view('user/forecast_cetak', $data);
	}
#=========================================================================================
#---------------------------------------report_forecasting--------------------------------
#=========================================================================================
}
