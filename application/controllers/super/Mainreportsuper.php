<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainreportsuper extends CI_Controller {

	public function __construct(){
		parent::__construct();	
		
		$this->load->model("user/main_user", "mu");
		$this->load->model("super/main_super", "ms");
		$this->load->library("response_message");

		if($this->session->userdata("double_log")["is_log"] != 1){
            redirect(base_url());
        }else{
        	if($this->session->userdata("double_log")["jenis_admin"] != 0){
                redirect(base_url());
            }
        }

	}

	public function index(){
		$data["page"] = "laporan_penjualan";
		$this->load->view('index_admin',$data);
	}

	public function index_penjualan(){
		$data["page"] = "laporan_penjualan";
		$data["toko"] = $this->ms->get_toko(); 

		$data_all = $this->ms->get_super_laporan_all();

		if(isset($_POST["tipe_choose"])){
			$tipe_choose 	= $this->input->post("tipe_choose");
			$periode 		= $this->input->post("periode");
			$th 			= $this->input->post("th");
			$id_toko		= $this->input->post("cabang");

			$data_all = $this->ms->get_super_laporan_toko($id_toko);	
			if($tipe_choose == "1"){
				$data_all = $this->ms->get_super_laporan_where($id_toko, $periode, $th);	
			}
		}

		
		$data["month"] 		= array("","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
		$data["penjualan"] 	= $data_all;
		print_r($data);

		$this->load->view('index_admin',$data);
	}

#===========================================================================================
#--------------------------------------- main_penjualan ------------------------------------
#===========================================================================================
	
	public function show_report(){
		$tipe_choose 	= $this->input->post("tipe_choose");
		$periode 		= $this->input->post("periode");
		$th 			= $this->input->post("th");
		$id_toko		= $this->input->post("cabang");

		$data_all = $this->ms->get_super_laporan_toko($id_toko);	
		if($tipe_choose == "1"){
			$data_all = $this->ms->get_super_laporan_where($id_toko, $periode, $th);	
		}

		$data["penjualan"] = $data_all;


		print_r($data_all);
		// $this->load->view('super/report_penjualan_cetak', $data);
	}

#===========================================================================================
#--------------------------------------- main_penjualan ------------------------------------
#===========================================================================================

#===========================================================================================
#--------------------------------------- main_peramalan ------------------------------------
#===========================================================================================
	public function index_double(){
		$data["page"] = "laporan_peramalan";
		$data["toko"] = $this->ms->get_toko();
		$data["data_analisis"] = "";
		$data["data_forecast"] = ""; 

		$this->load->view('index_admin',$data);
	}


	public function index_search(){
		$tipe_choose 	= $this->input->post("tipe_choose");
		$periode 		= $this->input->post("periode");
		$th 			= $this->input->post("th");
		$id_toko		= $this->input->post("cabang");

		$data_all = $this->ms->get_super_laporan_toko($id_toko);	
		if($tipe_choose == "1"){
			$data_all = $this->ms->get_super_laporan_where($id_toko, $periode, $th);	
		}


		$data_alpha = $this->mu->get_setting_default(array("sts_default"=>"1"));

		if(empty($data_alpha)){
			$data_alpha = $this->mu->get_alpha_first();
		}
		$alpha 	= (double)$data_alpha["alpha"];
		$beta 	= (double)$data_alpha["beta"];

		$p_forecast = 5;

		$array_analisis = array();

		$no = 1;
		$key_before = 0;
		foreach ($data_all as $key => $value) {
			if($no == 1){
				$array_analisis[$no] = array(
										"tgl"=>$value->tgl,
										"t"=>$no,
										"yt"=>$value->penjualan,
										"lt"=>$value->penjualan,
										"tt"=>0,
										"yt_est"=>$value->penjualan,
										"residual"=>0
									);
			}else {
				$lt = $alpha*(double)$value->penjualan+(1-$alpha)*((double)$array_analisis[$no-1]["lt"] + (double)$array_analisis[$no-1]["tt"]);

				$tt = $beta * ($lt - (double)$array_analisis[$no-1]["lt"]) + (1 - $beta) * (double)$array_analisis[$no-1]["tt"];

				$yt_est = $array_analisis[$no-1]["yt"];
				if($array_analisis[$no-1]["tt"] != 0){
					$yt_est = (double)$array_analisis[$no-1]["lt"] + 1 * (double)$array_analisis[$no-1]["tt"];
				}

				$residual = (double)$value->penjualan - $yt_est;

				$array_analisis[$no] = array(
										"tgl"=>$value->tgl,
										"t"=>$no,
										"yt"=>$value->penjualan,
										"lt"=>$lt,

										"tt"=>$tt,
										"yt_est"=>$yt_est,
										"residual"=>$residual
									);

			}

			$lt_end = $array_analisis[$no]["lt"];
			$tt_end = $array_analisis[$no]["tt"];
			$tgl_end= $value->tgl;

			$no++;			
		}

		$array_forecast = array();
		if($array_analisis){
			for ($i=1; $i <= $p_forecast ; $i++) { 
			$yt_est = (double)$lt_end + $i * (double)$tt_end;

			$array_forecast[$i] = array(
										"tgl"=>date('Y-m-d', strtotime('+'.$i.' days', strtotime($tgl_end))),
										"t"=>$i,
										"yt"=>0,
										"lt"=>0,

										"tt"=>0,
										"yt_est"=>$yt_est,
										"residual"=>0
									);
			}
		}
		

		// print_r("<pre>");
		// print_r($_POST);
		// print_r($data_all);
		// print_r($array_analisis);
		// print_r($array_forecast);
		// print_r($data_alpha);
		$data["page"] = "laporan_peramalan";
		$data["toko"] = $this->ms->get_toko();
		$data["data_analisis"] = $array_analisis;
		$data["data_forecast"] = $array_forecast;

		$this->load->view("index_admin", $data);

		
	}

	public function cetak_peramalan(){
		$tipe_choose 	= $this->input->post("tipe_choose");
		$periode 		= $this->input->post("periode");
		$th 			= $this->input->post("th");
		$id_toko		= $this->input->post("cabang");

		$data_all = $this->ms->get_super_laporan_toko($id_toko);	
		if($tipe_choose == "1"){
			$data_all = $this->ms->get_super_laporan_where($id_toko, $periode, $th);	
		}


		$data_alpha = $this->mu->get_setting_default(array("sts_default"=>"1"));

		if(empty($data_alpha)){
			$data_alpha = $this->mu->get_alpha_first();
		}
		$alpha 	= (double)$data_alpha["alpha"];
		$beta 	= (double)$data_alpha["beta"];

		$p_forecast = 5;

		$array_analisis = array();

		$no = 1;
		$key_before = 0;
		foreach ($data_all as $key => $value) {
			if($no == 1){
				$array_analisis[$no] = array(
										"tgl"=>$value->tgl,
										"t"=>$no,
										"yt"=>$value->penjualan,
										"lt"=>$value->penjualan,
										"tt"=>0,
										"yt_est"=>$value->penjualan,
										"residual"=>0
									);
			}else {
				$lt = $alpha*(double)$value->penjualan+(1-$alpha)*((double)$array_analisis[$no-1]["lt"] + (double)$array_analisis[$no-1]["tt"]);

				$tt = $beta * ($lt - (double)$array_analisis[$no-1]["lt"]) + (1 - $beta) * (double)$array_analisis[$no-1]["tt"];

				$yt_est = $array_analisis[$no-1]["yt"];
				if($array_analisis[$no-1]["tt"] != 0){
					$yt_est = (double)$array_analisis[$no-1]["lt"] + 1 * (double)$array_analisis[$no-1]["tt"];
				}

				$residual = (double)$value->penjualan - $yt_est;

				$array_analisis[$no] = array(
										"tgl"=>$value->tgl,
										"t"=>$no,
										"yt"=>$value->penjualan,
										"lt"=>$lt,

										"tt"=>$tt,
										"yt_est"=>$yt_est,
										"residual"=>$residual
									);

			}

			$lt_end = $array_analisis[$no]["lt"];
			$tt_end = $array_analisis[$no]["tt"];
			$tgl_end= $value->tgl;

			$no++;			
		}

		$array_forecast = array();
		if($array_analisis){
			for ($i=1; $i <= $p_forecast ; $i++) { 
			$yt_est = (double)$lt_end + $i * (double)$tt_end;

			$array_forecast[$i] = array(
										"tgl"=>date('Y-m-d', strtotime('+'.$i.' days', strtotime($tgl_end))),
										"t"=>$i,
										"yt"=>0,
										"lt"=>0,

										"tt"=>0,
										"yt_est"=>$yt_est,
										"residual"=>0
									);
			}
		}
		

		// print_r("<pre>");
		// print_r($_POST);
		// print_r($data_all);
		// print_r($array_analisis);
		// print_r($array_forecast);
		// print_r($data_alpha);
		$data["page"] = "laporan_peramalan";
		$data["toko"] = $this->ms->get_toko();
		$data["data_analisis"] = $array_analisis;
		$data["data_forecast"] = $array_forecast;

		$this->load->view('user/forecast_cetak', $data);

		
	}
#===========================================================================================
#--------------------------------------- main_peramalan ------------------------------------
#===========================================================================================




}
