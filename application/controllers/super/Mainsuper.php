<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainsuper extends CI_Controller {

	public function __construct(){
		parent::__construct();	
		
		$this->load->model("super/main_super", "ms");
		$this->load->library("response_message");

		// if($this->session->userdata("double_log")["is_log"] != 1){
  //           redirect(base_url());
  //       }else{
  //       	if($this->session->userdata("double_log")["jenis_admin"] != 0){
  //               redirect(base_url());
  //           }
  //       }
	}

	public function index(){
		$data["page"] = "home";
		$this->load->view('index_admin', $data);
	}

	public function index_admin(){
		$data["page"] = "admin";
		$data["admin"] = $this->ms->get_admin();
		$data["toko"] = $this->ms->get_toko();

		$this->load->view('index_admin', $data);
	}

	public function index_toko(){
		$data["page"] = "toko";
		$data["toko"] = $this->ms->get_toko();
		
		$this->load->view('index_admin', $data);
	}

	public function index_alpha(){
		$data["page"] = "alpha";
		$data["alpha"] = $this->ms->get_all("seting_alpha");
		
		$this->load->view('index_admin', $data);
	}

#===========================================================================================
#--------------------------------------- Main Admin-----------------------------------------
#===========================================================================================
	
	private function validation_ins_admin(){
		$config_val_input = array(
            array(
                'field'=>'nama',
                'label'=>'Nama',
                'rules'=>'required|alpha_numeric_spaces',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                 )
                       
            ),
            array(
                'field'=>'nip',
                'label'=>'NIP',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'user',
                'label'=>'User Name',
                'rules'=>'required|alpha_numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'alpha_numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                 )
                       
            ),
            array(
                'field'=>'pass',
                'label'=>'Password',
                'rules'=>'required|alpha_numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'alpha_numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                 )
                       
            ),
            array(
                'field'=>'repass',
                'label'=>'Ulangi Password',
                'rules'=>'required|alpha_numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'alpha_numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_admin(){
		if($this->validation_ins_admin()){
			$nama = $this->input->post("nama");
			$nip = $this->input->post("nip");
			$user = $this->input->post("user");
			$jenis_admin = $this->input->post("jenis_admin");
			$jenis_toko = $this->input->post("jenis_toko");
			$pass = $this->input->post("pass");
			$repass = $this->input->post("repass");

			if($pass == $repass){
				$insert = $this->db->query("select insert_admin('".$user."','".$pass."','".$nama."','".$nip."','".$jenis_toko."','".$jenis_admin."')");

				if($insert){

				}else {

				}
			}

		}

		redirect(base_url()."super/admin");
	}

	public function index_up_admin(){
		$id_admin = $this->input->post("id_admin");
		$data["status"] = false;
		$data["val"] = null;
			
		if(!empty($this->ms->get_admin_where(array("id_admin"=>$id_admin)))){
			$data["status"] = true;
			$data["val"] = $this->ms->get_admin_where(array("id_admin"=>$id_admin));
		}

		print_r(json_encode($data));
	}

	private function validation_up_admin(){
		$config_val_input = array(
            array(
                'field'=>'nama',
                'label'=>'Nama',
                'rules'=>'required|alpha_numeric_spaces',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                 )
                       
            ),
            array(
                'field'=>'nip',
                'label'=>'NIP',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'user',
                'label'=>'User Name',
                'rules'=>'required|alpha_numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'alpha_numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}


	public function up_admin(){
		// print_r("<pre>");
		// print_r($_POST);
		if($this->validation_up_admin()){
			$nama = $this->input->post("nama");
			$nip = $this->input->post("nip");
			$user = $this->input->post("user");
			$jenis_admin = $this->input->post("jenis_admin");
			$id_toko = $this->input->post("jenis_toko");

			$id_admin = $this->input->post("id_admin");

			// $pass = $this->input->post("pass");
			// $repass = $this->input->post("repass");
			$data = array(
						"nama"=>$nama,
						"nip"=>$nip,
						"email"=>$user,
						"jenis_admin"=>$jenis_admin,
						"id_toko"=>$id_toko,
					);
			
			$where = array(
						"id_admin"=>$id_admin
					);

			$update = $this->ms->update_admin($data, $where);

			if($update){
				echo "up";
			}else {
				echo "fail";
			}
		}
		redirect(base_url()."super/admin");
	}

	public function delete_admin($id_admin){
		// $id_admin = $this->input->post("id_admin");

		$delete = $this->ms->delete_admin(array("id_admin"=>$id_admin));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."super/admin");
	}
#===========================================================================================
#--------------------------------------- Main Admin-----------------------------------------
#===========================================================================================

#===========================================================================================
#--------------------------------------- Main Toko------------------------------------------
#===========================================================================================

	private function validation_ins_toko(){
		$config_val_input = array(
            array(
                'field'=>'nama',
                'label'=>'Nama',
                'rules'=>'required|alpha_numeric_spaces',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                 )
                       
            ),
            array(
                'field'=>'alamat',
                'label'=>'Alamat',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_toko(){
		if($this->validation_ins_toko()){
			$nama = $this->input->post("nama");
			$alamat = $this->input->post("alamat");

			$send_ins = array("id_toko"=>"",
							"cabang"=>$nama,
							"alamat"=>$alamat);

			$insert = $this->ms->insert_toko($send_ins);
			if($insert){

			}else {

			}
			
		}

		redirect(base_url()."super/toko");
	}

	public function index_up_toko(){
		$id_toko = $this->input->post("id_toko");
		$data["status"] = false;
		$data["val"] = null;
			
		if(!empty($this->ms->get_toko_where(array("id_toko"=>$id_toko)))){
			$data["status"] = true;
			$data["val"] = $this->ms->get_toko_where(array("id_toko"=>$id_toko));
		}

		print_r(json_encode($data));
	}


	public function up_toko(){
		if($this->validation_ins_toko()){
			$nama = $this->input->post("nama");
			$alamat = $this->input->post("alamat");

			$id_toko = $this->input->post("id_toko");

			$where_ins = array(
							"id_toko"=>$id_toko);

			$send_ins = array(
							"cabang"=>$nama,
							"alamat"=>$alamat);

			$update = $this->ms->update_toko($send_ins, $where_ins);
			if($update){

			}else {

			}
			
		}

		redirect(base_url()."super/toko");
	}

	public function delete_toko($id_toko){
		// $id_admin = $this->input->post("id_admin");

		$delete = $this->ms->delete_toko(array("id_toko"=>$id_toko));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."super/toko");
	}
#===========================================================================================
#--------------------------------------- Main Admin-----------------------------------------
#===========================================================================================

#===========================================================================================
#--------------------------------------- Main alpha-----------------------------------------
#===========================================================================================
	private function validation_ins_alpha(){
		$config_val_input = array(
            array(
                'field'=>'alpha',
                'label'=>'alpha',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'beta',
                'label'=>'beta',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_alpha(){
		if($this->validation_ins_alpha()){
			$alpha = $this->input->post("alpha");
			$beta = $this->input->post("beta");
			$sts_default = $this->input->post("default");

			$send_ins = array("id_setting"=>"",
							"alpha"=>$alpha,
							"beta"=>$beta,
							"sts_default"=>$sts_default);

			$insert = $this->ms->insert_data("seting_alpha" ,$send_ins);
			if($insert){
				echo "true";
			}else {
				echo "false";
			}
			
		}

		redirect(base_url()."super/alpha_uji");
	}

	public function index_up_alpha(){
		$id_setting = $this->input->post("id_setting");
		$data["status"] = false;
		$data["val"] = null;
			
		if(!empty($this->ms->get_where("seting_alpha", array("id_setting"=>$id_setting)))){
			$data["status"] = true;
			$data["val"] = $this->ms->get_where("seting_alpha", array("id_setting"=>$id_setting));
		}

		print_r(json_encode($data));
	}

	public function up_alpha(){
		if($this->validation_ins_alpha()){
			$alpha = $this->input->post("alpha");
			$beta = $this->input->post("beta");
			$sts_default = $this->input->post("default");

			$id_setting = $this->input->post("id_setting");

			$where_ins = array(
							"id_setting"=>$id_setting);

			$send_ins = array(
							"alpha"=>$alpha,
							"beta"=>$beta,
							"sts_default"=>$sts_default);

			$update = $this->ms->update_data("seting_alpha", $send_ins, $where_ins);
			if($update){
				echo "true";
			}else {
				echo "false";
			}
			
		}

		redirect(base_url()."super/alpha_uji");
	}

	public function delete_alpha($id_setting){
		$delete = $this->ms->delete_data("seting_alpha", array("id_setting"=>$id_setting));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."super/alpha_uji");
	}
#===========================================================================================
#--------------------------------------- Main alpha-----------------------------------------
#===========================================================================================





}
