<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainlogin extends CI_Controller {

	public function __construct(){
		parent::__construct();	
		$this->load->model('Main_login', 'ml');
		
        $this->load->library("response_message");

        if(isset($_SESSION["double_log"])) {
            if($this->session->userdata("double_log")["is_log"] == 1){
                if($this->session->userdata("double_log")["jenis_admin"] == 1){
                    redirect(base_url()."admin/home");
                }else {
                    redirect(base_url()."super/home");
                }
            }   
        }
	}
    
    public function index(){
        $this->load->view('login_admin');
    }

    public function auth(){
    	print_r("<pre>");
        if($this->val_form_log()){
            $email = $this->input->post('email');
    		$password = $this->input->post('password');
    		$where = array(
    			'email' => $email,
    			'password' => md5($password)
    			);

    		// print_r($where);
    		
            $cek = $this->ml->get_admin($where)->row_array();

            // print_r($cek);
            
    		if(!empty($cek)){
                $cek["is_log"] = 1;

                //session
                $this->session->set_userdata("double_log",$cek);
               

                //msg_session
                $main_msg = array("status" => true, "msg"=>$this->response_message->get_success_msg("LOG_SUC"));
                $msg_array = $this->response_message->default_mgs($main_msg,null);
                $this->session->set_flashdata("response_login", $msg_array);         
                

                if($cek["jenis_admin"] == 1){
                    redirect(base_url()."admin/home");
                }else {
                    redirect(base_url()."super/home");
                }
                
    		}else{
                
                //msg_session                
                $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("LOG_FAIL"));
                $msg_array = $this->response_message->default_mgs($main_msg,null);
                $this->session->set_flashdata("response_login", $msg_array); 

                // redirect(base_url("admin/login"));        
    		}     
        }else{
            $msg_detail = array(
                                "email" => form_error("email"),
                                "password" => form_error("password")  
                            );
                            
            $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("LOG_FAIL"));
            $msg_array = $this->response_message->default_mgs($main_msg,$msg_detail);
            $this->session->set_flashdata("response_login", $msg_array);
            // redirect(base_url("admin/login"));
        }
        
        redirect(base_url("admin/login"));
        // print_r($_SESSION);
        // print_r($msg_array);
        
    }
    
    private function val_form_log(){
        $config_val_input = array(
                array(
                    'field'=>'email',
                    'label'=>'Username',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                array(
                    'field'=>'password',
                    'label'=>'Password',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }
}
