<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_super extends CI_Model {

	public function get_admin(){
		$this->db->join("toko tk", "ad.id_toko = tk.id_toko");
		$data = $this->db->get("admin ad")->result();

		return $data;
	}

	public function get_admin_where($where){
		$this->db->join("toko tk", "ad.id_toko = tk.id_toko");
		$data = $this->db->get_where("admin ad", $where)->row_array();

		return $data;
	}

	public function insert_admin($data){
		$insert = $this->db->insert("admin",$data);
		return $insert;
	}

	public function update_admin($data, $where){
		$update = $this->db->update("admin", $data, $where);
		return $update;
	}

	public function delete_admin($where){
		$delete = $this->db->delete("admin",$where);
		return $delete;
	}



	public function get_toko(){
		$data = $this->db->get("toko")->result();

		return $data;
	}

	public function get_toko_where($where){
		$data = $this->db->get_where("toko", $where)->row_array();

		return $data;
	}

	public function insert_toko($data){
		$insert = $this->db->insert("toko", $data);

		return $insert;
	}

	public function update_toko($set, $where){
		$update = $this->db->update("toko", $set, $where);

		return $update;
	}

	public function delete_toko($where){
		$delete =  $this->db->delete("toko", $where);

		return $delete;
	}

	public function get_super_laporan_where($id_toko, $periode, $th){
		
		$this->db->join("admin ad", "lp.id_admin = ad.id_admin");
		$this->db->join("toko tk", "ad.id_toko = tk.id_toko");
		$this->db->where("DATE_FORMAT(tgl, '%m') = ".$periode);
		$this->db->where("DATE_FORMAT(tgl, '%Y') = ".$th);
		$this->db->order_by("tgl", "asc");
		$data = $this->db->get_where("pendapatan lp", array("tk.id_toko"=>$id_toko))->result();
		return $data;
	}

	public function get_super_laporan_toko($id_toko){
		
		$this->db->join("admin ad", "lp.id_admin = ad.id_admin");
		$this->db->join("toko tk", "ad.id_toko = tk.id_toko");
		$this->db->order_by("tgl", "asc");
		$data = $this->db->get_where("pendapatan lp", array("tk.id_toko"=>$id_toko))->result();
		return $data;
	}

	public function get_super_laporan_all(){
		$this->db->join("admin ad", "lp.id_admin = ad.id_admin");
		$this->db->join("toko tk", "ad.id_toko = tk.id_toko");
		$this->db->order_by("tgl", "asc");
		$data = $this->db->get("pendapatan lp")->result();
		return $data;
	}



	public function get_all($table){
		$data = $this->db->get($table)->result();

		return $data;
	}

	public function get_where($table, $where){
		$data = $this->db->get_where($table, $where)->row_array();

		return $data;
	}

	public function insert_data($table, $data){
		$insert = $this->db->insert($table, $data);
		return $insert;
	}

	public function update_data($table, $data, $where){
		$update = $this->db->update($table, $data, $where);
		return $update;
	}

	public function delete_data($table, $where){
		$delete = $this->db->delete($table, $where);
		return $delete;
	}
}
