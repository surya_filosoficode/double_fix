<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_user extends CI_Model {

	public function get_laporan_all($id_admin){
		
		$this->db->join("admin ad", "lp.id_admin = ad.id_admin");
		$this->db->join("toko tk", "ad.id_toko = tk.id_toko");
		// $this->db->where("DATE_FORMAT(tgl, '%m') = ".$periode."");
		// $this->db->where("DATE_FORMAT(tgl, '%Y') = '".$th."'");
		$this->db->order_by("lp.periode", "asc");
		$data = $this->db->get_where("pendapatan lp", array("lp.id_admin"=>$id_admin))->result();
		return $data;
	}

	public function get_laporan_all_desc($id_admin){
		
		$this->db->join("admin ad", "lp.id_admin = ad.id_admin");
		$this->db->join("toko tk", "ad.id_toko = tk.id_toko");
		// $this->db->where("DATE_FORMAT(tgl, '%m') = ".$periode."");
		// $this->db->where("DATE_FORMAT(tgl, '%Y') = '".$th."'");
		$this->db->order_by("lp.tgl", "desc");
		$data = $this->db->get_where("pendapatan lp", array("lp.id_admin"=>$id_admin))->result();
		return $data;
	}

	public function get_laporan_where($id_admin, $periode, $th){
		
		$this->db->join("admin ad", "lp.id_admin = ad.id_admin");
		$this->db->join("toko tk", "ad.id_toko = tk.id_toko");
		$this->db->where("CAST(substr(periode, 5, 2) as UNSIGNED) = ".$periode);
		$this->db->where("Left(periode, 4) =".$th);
		$this->db->order_by("tgl", "asc");
		$data = $this->db->get_where("pendapatan lp", array("lp.id_admin"=>$id_admin))->result();
		return $data;
	}

	public function get_laporan_where_id($id_penjualan){
		
		$this->db->join("admin ad", "lp.id_admin = ad.id_admin");
		$this->db->join("toko tk", "ad.id_toko = tk.id_toko");
		$this->db->order_by("tgl", "asc");
		$data = $this->db->get_where("pendapatan lp", array("lp.id_lap"=>$id_penjualan))->row_array();
		return $data;
	}

	public function get_laporan_where_index_update($id_penjualan){
		$this->db->select("id_lap, lp.tgl, lp.penjualan, lp.periode");
		$this->db->join("admin ad", "lp.id_admin = ad.id_admin");
		$this->db->join("toko tk", "ad.id_toko = tk.id_toko");
		$this->db->order_by("tgl", "asc");
		$data = $this->db->get_where("pendapatan lp", array("lp.id_lap"=>$id_penjualan))->row_array();
		return $data;
	}

	public function insert_laporan($data){
		$data = $this->db->insert("pendapatan", $data);
		return $data;
	}

	public function update_laporan($data, $where){
		$data = $this->db->update("pendapatan", $data, $where);
		return $data;
	}

	public function delete_laporan($where){
		$delete = $this->db->delete("pendapatan", $where);
		return $delete;
	}

	public function get_alpha($where){
		$get_alpha = $this->db->get_where("seting_alpha", $where)->row_array();
		return $get_alpha;
	}

	public function get_alpha_first(){
		$this->db->order_by("id_setting", "asc");
		$get_alpha = $this->db->get("seting_alpha")->row_array();
		return $get_alpha;
	}

	public function get_setting_default($where){
		$get_alpha = $this->db->get_where("seting_alpha", $where)->row_array();
		return $get_alpha;
	}

	public function check_periode($where){
		$data = $this->db->get_where("pendapatan", $where)->result();
		return $data;

	}

}
