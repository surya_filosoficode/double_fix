                <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Data Single Exponensial Forecasting</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    
                </div>
                
            </div> -->
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">            
                <br>
                <!-- Row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-outline-inverse">                
                            <!-------------------------------------------Data Table------------------------------------>
                            <div class="card-header">
                                <h4 class="m-b-0 text-white">Data Pendapatan</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive m-t-40">
                                    <table id="example24" border="1" width="50%" style="border-collapse: collapse;">
                                        <thead>
                                            <tr>
                                                <th width="10%">Periode</th>
                                                <th width="15%">Tgl.</th>
                                                <th width="30%">Nama Toko</th>
                                                <th width="15%">Penjualan Real</th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody id="output_laporan">
                                            <?php
                                                if(!empty($penjualan)){
                                                    $sum_penjualan = 0;
                                                    foreach ($penjualan as $r_penjualan => $v_penjualan){
                                                        $sum_penjualan += $v_penjualan->penjualan;
                                                        echo "<tr>
                                                                <td align=\"center\">".($r_penjualan+1)."</td>
                                                                <td align=\"center\">".$v_penjualan->tgl."</td>
                                                                <td align=\"center\">".$v_penjualan->cabang."</td>
                                                                <td align=\"right\">Rp. ".number_format($v_penjualan->penjualan, 2,'.', ',')."</td>
                                                                
                                                            </tr>";
                                                    }
                                                }
                                            ?>
                                        </tbody>

                                        <tfoot>
                                            <tr>
                                                <td colspan="3" align="center"><b>Jumlah</b></td>
                                                <td align="right"><b><?php print_r("Rp. ".number_format($sum_penjualan));?></b></td>
                                                
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
                <!-- Row -->
            </div>

            