
                                    
                                        <thead>
                                            <tr>
                                                <th>Tanggal</th>
                                                <th>Sales Net</th>
                                                <th>Struk</th>
                                                <th>Pergantian</th>
                                                <th>Variance</th>
                                                <th>Discount</th>
                                                <th>DSI Hari Ini</th>
                                                
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Tanggal</th>
                                                <th>Sales Net</th>
                                                <th>Struk</th>
                                                <th>Pergantian</th>
                                                <th>Variance</th>
                                                <th>Discount</th>
                                                <th>DSI Hari Ini</th>

                                            </tr>
                                        </tfoot>
                                        <tbody id="output_laporan">
                                            <?php

                                            if(!empty($penjualan)){
                                                 foreach ($penjualan as $r_data => $v_data) {
                                                    echo "<tr>
                                                            <td>".(explode("-", $v_data->tgl)[2]+0)."</td>
                                                            <td align=\"right\">Rp. ".number_format($v_data->sales_net, 2,'.', ',')."</td>
                                                            <td align=\"right\">".number_format($v_data->struk, 0,'.', ',')."</td>
                                                            <td align=\"right\">Rp. ".number_format($v_data->pergantian, 2,'.', ',')."</td>
                                                            <td align=\"right\">Rp. ".number_format($v_data->variance, 2,'.', ',')."</td>
                                                            <td align=\"right\">Rp. ".number_format($v_data->discount, 2,'.', ',')."</td>
                                                            <td align=\"right\">Rp. ".number_format($v_data->dsi_ini, 2,'.', ',')."</td>
                                                        </tr>";
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    <!-- </table> -->