            
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Toko</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    
                </div>
                <div>
                    <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">            
                <div class="row">
                    <!-- <div class="row"> -->
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-lg-10">
                                            <h4 class="card-title">Data Toko</h4>
                                        </div>
                                        <div class="col-lg-2">
                                            <button type="button" class="btn btn-success waves-effect text-right" data-toggle="modal" data-target="#insert_modal">Tambah Toko</button>
                                        </div>
                                    </div>
                                    
                                    <div class="table-responsive m-t-40">
                                        <table id="myTable" class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Id Toko</th>
                                                    <th>Nama Cabang</th>
                                                    <th>Alamat Cabang</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    if(!empty($toko)){
                                                        foreach ($toko as $r_toko => $v_toko) {
                                                            
                                                            echo "<tr>
                                                                    <td>".$v_toko->id_toko."</td>
                                                                    <td>".$v_toko->cabang."</td>
                                                                    <td>".$v_toko->alamat."</td>
                                                                    
                                                                    <td align=\"center\">
                                                                        <a href=\"#\" onclick=\"up_toko('".$v_toko->id_toko."')\"><i class=\"btn btn-success\"><i class=\"fa fa-pencil-square-o\"></i></i></a>
                                                                        <a href=\"#\" onclick=\"del_toko('".$v_toko->id_toko."')\"><i class=\"btn btn-danger\"><i class=\"fa fa-trash-o\"></i></i></a>
                                                                    </td>
                                                                </tr>";
                                                        }
                                                    }
                                                ?>
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    <!-- </div> -->
                </div>
                <!-- End Row -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
                                <div class="modal fade bs-example-modal-lg" id="insert_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myLargeModalLabel">Tambah Data Toko</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <form action="<?= base_url()."super/mainsuper/insert_toko"?>" method="post" class="form-horizontal">
                                            <div class="modal-body">
                                                <br>
                                                <div class="form-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group row">
                                                                <label class="control-label text-left col-md-3">Nama Cabang</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama" required="">
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="form-group row">
                                                                <label class="control-label text-left col-md-3">Alamat</label>
                                                                <div class="col-md-9">
                                                                    <textarea class="form-control" id="alamat" name="alamat" placeholder="Alamat" required=""></textarea>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>
                                                    <!--/row-->
                                                </div>    
                                                                                               
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-danger waves-effect text-left">Sipman</button>
                                            </div>
                                            </form>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->



                                <div class="modal fade bs-example-modal-lg" id="update_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myLargeModalLabel">Ubah Data Toko</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <form action="<?= base_url()."super/mainsuper/up_toko"?>" method="post" class="form-horizontal">
                                            <div class="modal-body">
                                                <br>
                                                <div class="form-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group row">
                                                                <label class="control-label text-left col-md-3">Nama Cabang</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" id="up_nama" name="nama" placeholder="Nama" required="">
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="form-group row">
                                                                <label class="control-label text-left col-md-3">Alamat</label>
                                                                <div class="col-md-9">
                                                                    
                                                                    <textarea class="form-control" id="up_alamat" name="alamat" placeholder="Alamat" required=""></textarea>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12" hidden="">
                                                            <div class="form-group row">
                                                                <label class="control-label text-left col-md-3">Id Toko</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" id="up_id_toko" name="id_toko" placeholder="Id Toko" readonly="" required="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>
                                                    <!--/row-->
                                                </div>                                             
                                            </div>
                                            <div class="modal-footer">
                                                <!-- <input type="submit" name="ubah" class="btn btn-danger text-left"" value="Ubah"> -->
                                                <button type="submit" id="btn_ubah" class="btn btn-danger waves-effect text-left">Ubah</button>
                                            </div>
                                            </form>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->



                                <script type="text/javascript">
                                    function del_toko(id_toko){
                                        var conf = confirm("Apakah anda yakin untuk menghapus "+id_toko+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_toko+" akan terhapus semau.. !!!!!!! ");

                                        if(conf){
                                            window.location.href = "<?= base_url()."super/mainsuper/delete_toko/";?>"+id_toko;
                                        }else{

                                        }
                                    }


                                    function up_toko(id_toko){
                                        clear_mod_up();
                                        // console.lo

                                        var data_main =  new FormData();
                                        data_main.append('id_toko', id_toko);    
                                        $.ajax({
                                            url: "<?php echo base_url()."/super/mainsuper/index_up_toko/";?>", // point to server-side PHP script 
                                            dataType: 'html',  // what to expect back from the PHP script, if anything
                                            cache: false,
                                            contentType: false,
                                            processData: false,
                                            data: data_main,                         
                                            type: 'post',
                                            success: function(res){
                                                // console.log(res);
                                                res_update(res);
                                                // $("#out_up_mhs").html(res);
                                            }
                                        });
                                        $("#update_modal").modal('show');
                                        
                                    }

                                    function res_update(res){
                                        var data = JSON.parse(res);
                                        // console.log(data);

                                        if(data.status){
                                            var main_data = data.val;
                                            console.log(main_data);
                                            
                                            $("#up_nama").val(main_data.cabang);
                                            $("#up_alamat").val(main_data.alamat);
                                            $("#up_id_toko").val(main_data.id_toko);
                                           
                                        }else{
                                            clear_mod_up();
                                        }
                                    }

                                    function clear_mod_up(){
                                        $("#up_nama").val("");
                                        $("#up_alamat").val("");
                                        $("#up_id_toko").val("");

                                    }


                                    
                                </script>