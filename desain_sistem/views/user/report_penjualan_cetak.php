                <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Data Single Exponensial Forecasting</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    
                </div>
                
            </div> -->
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">            
                <br>
                <!-- Row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-outline-inverse">                
                            <!-------------------------------------------Data Table------------------------------------>
                            <div class="card-header">
                                <h4 class="m-b-0 text-white">Data Pembelian Bahan Baku</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive m-t-40">
                                    <table id="example24" border="1" width="100%" style="border-collapse: collapse;">
                                        <thead>
                                            <tr>
                                                <th width="10%">No. </th>
                                                <th width="30%">Tanggal Pembelian Bahan Baku</th>
                                                <th width="40%">Periode Penggunaan Bahan Baku</th>
                                                <th width="20%">Total Pembelian</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>No. </th>
                                                <th>Tanggal Pembelian Bahan Baku</th>
                                                <th>Periode Penggunaan Bahan Baku</th>
                                                <th>Total Pembelian</th>
                                                
                                            </tr>
                                        </tfoot>
                                        <tbody id="output_laporan">
                                            <?php
                                                if(!empty($penjualan)){
                                                    foreach ($penjualan as $r_penjualan => $v_penjualan){
                                                        $str_periode = "minggu ke-".substr($v_penjualan->periode, 6, 1).", ".$month[(int)substr($v_penjualan->periode, 4, 2)]."/". substr($v_penjualan->periode, 0, 4);
                                                        echo "<tr>
                                                                <td align=\"center\">".($r_penjualan+1).". </td>
                                                                <td align=\"center\">".$v_penjualan->tgl."</td>
                                                                <td align=\"center\">".$str_periode."</td>
                                                                <td align=\"right\">".number_format($v_penjualan->penjualan, 2,'.', ',')." Kg.</td>
                                                                
                                                            </tr>";
                                                    }
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
                <!-- Row -->
            </div>


            <script type="text/javascript">
                window.print();
            </script>
            