            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Beranda</h3>
                </div>
                <div>
                    <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                </div>
            </div>

            <?php
                // $data["page"] = "peramalan";
                // $data["data_analisis"] = count($array_analisis);
                // $data["data_forecast"] = $array_forecast;

                // $data["data_json"]  = json_encode($array_json);

                $c_data_analisis = 0;

                $c_data_forecast = array();

                $c_data_json = "{}";

                if($data_analisis){
                    $c_data_analisis = $data_analisis;
                }

                if($data_forecast){
                    $c_data_forecast = $data_forecast;
                }

                if($data_json){
                    $c_data_json = $data_json;
                }

            ?>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <div class="col-lg-4 col-xlg-3">
                        <div class="card card-inverse card-info">
                            <div class="card-body">
                                <div class="d-flex">
                                    <div class="m-r-20 align-self-center">
                                        <h1 class="text-white"><i class="ti-light-bulb"></i></h1></div>
                                    <div>
                                        <h3 class="card-title">Total Data yang Digunakan</h3>
                                        <h6 class="card-subtitle"></h6> </div>
                                </div>
                                <div class="row">
                                    <div class="col-6 align-self-center">
                                        <h2 class="font-light text-white"><?=$data_analisis?></h2>
                                    </div>
                                    <div class="col-6 p-t-10 p-b-20 text-right">
                                        <div class="spark-count" style="height:65px"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-xlg-3">
                        <div class="card card-inverse card-success">
                            <div class="card-body">
                                <div class="d-flex">
                                    <div class="m-r-20 align-self-center">
                                        <h1 class="text-white"><i class="ti-pie-chart"></i></h1></div>
                                    <div>
                                        <h3 class="card-title">Periode <br>Peramalan</h3>
                                        <h6 class="card-subtitle"></h6> </div>
                                </div>
                                <div class="row">
                                    <div class="col-6 align-self-center">
                                        <h2 class="font-light text-white">5 Periode</h2>
                                    </div>
                                    <div class="col-6 p-t-10 p-b-20 text-right align-self-center">
                                        <div class="spark-count2" style="height:65px"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    
                    <!-- Column -->
                    
                    <!-- Column -->
                </div>
                <!-- Row -->

                <div class="row">
                    <div class="col-lg-12 col-xlg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="d-flex flex-wrap">
                                            <div>
                                                <h4 class="card-title">Grafik Peramalan Pembelian Bahan Baku</h4>
                                            </div>
                                            <div class="ml-auto">
                                                <ul class="list-inline">
                                                    <li>
                                                        <h6 class="text-muted text-success"><i class="fa fa-circle font-10 m-r-10 "></i>Data Forecast</h6> </li>
                                                    <li>
                                                        <h6 class="text-muted  text-info"><i class="fa fa-circle font-10 m-r-10"></i>Data Real</h6> </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div id="earning" style="height: 355px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->

            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

             <!--morris JavaScript -->
    <script src="<?php print_r(base_url());?>template_admin/assets/plugins/raphael/raphael-min.js"></script>
    <script src="<?php print_r(base_url());?>template_admin/assets/plugins/morrisjs/morris.min.js"></script>
    <!-- Chart JS -->
    <!-- <script src="<?php print_r(base_url());?>template_admin/horizontal/js/dashboard1.js"></script> -->

    <script type="text/javascript">
        var data_main = JSON.parse('<?php print_r($c_data_json);?>');
        console.log(data_main);
        /*
        Template Name: Admin Press Admin
        Author: Themedesigner
        Email: niravjoshi87@gmail.com
        File: js
        */
        $(function() {
            "use strict";
            // ============================================================== 
            // Sales overview
            // ============================================================== 
            Morris.Area({
                element: 'earning',
                data: data_main,
                xkey: 'periode',
                ykeys: ['pembelian_real', 'estimasi_pembelian'],
                labels: ['Pembelian Real', 'Estimasi Pembelian'],
                pointSize: 3,
                fillOpacity: 0,
                pointStrokeColors: ['#1976d2', '#26c6da', '#1976d2'],
                behaveLikeLine: true,
                gridLineColor: '#e0e0e0',
                lineWidth: 3,
                hideHover: 'auto',
                lineColors: ['#1976d2', '#26c6da', '#1976d2'],
                resize: true

            });

            // ============================================================== 
            // Sales overview
            // ==============================================================
            // ============================================================== 
            // Download count
            // ============================================================== 
            var sparklineLogin = function() {
                $('.spark-count').sparkline([4, 5, 0, 10, 9, 12, 4, 9, 4, 5, 3, 10, 9, 12, 10, 9], {
                    type: 'bar',
                    width: '100%',
                    height: '70',
                    barWidth: '2',
                    resize: true,
                    barSpacing: '6',
                    barColor: 'rgba(255, 255, 255, 0.3)'
                });

                $('.spark-count2').sparkline([20, 40, 30], {
                    type: 'pie',
                    height: '90',
                    resize: true,
                    sliceColors: ['#1cadbf', '#1f5f67', '#ffffff']
                });
            }
            var sparkResize;

            sparklineLogin();


        });
    </script>