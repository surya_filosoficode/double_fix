                <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Data Single Exponensial Forecasting</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    
                </div>
                
            </div> -->
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">            
                <br>
                <!-- Row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-outline-inverse">                
                            <!-------------------------------------------Data Table------------------------------------>
                            <div class="card-header">

                                <h4 class="m-b-0 text-white">Data Perhitungan</h4>
                                <hr>
                                <br>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive m-t-40">
                                    <table id="example24" border="1" width="80%" style="border-collapse: collapse;">
                                        <thead>
                                            <tr>
                                                <th width="5%">No</th>
                                                <th width="20%">Periode</th>
                                                <th width="10%">Tgl. Pembelian</th>
                                                <th width="10%">Total Pembelian</th>
                                                <th width="10%">lt</th>
                                                <th width="15%">tt</th>
                                                <th width="15%">Analisa Peramalan</th>
                                                <th width="15%">Residual</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>No</th>
                                                <th>Periode</th>
                                                <th>Tgl. Pembelian</th>
                                                <th>Total Pembelian</th>
                                                <th>lt</th>
                                                <th>tt</th>
                                                <th>Analisa Peramalan</th>
                                                <th>Residual</th>
                                            </tr>
                                        </tfoot>
                                        <tbody id="output_laporan">
                                            <?php
                                                if($data_analisis){
                                                    $no = 1;
                                                    foreach ($data_analisis as $r_data_forecast => $v_data_forecast) {
                                                        // print_r("<pre>");
                                                        // print_r($v_data_forecast);
                                                        $str_periode = "minggu ke-".substr($v_data_forecast["periode"], 6, 1).", ".$month[(int)substr($v_data_forecast["periode"], 4, 2)]."/". substr($v_data_forecast["periode"], 0, 4);
                                                        print_r("<tr>
                                                                    <td align=\"center\">".$v_data_forecast["t"]."</td>
                                                                    <td align=\"right\">".$str_periode."</td>
                                                                    <td align=\"center\">".$v_data_forecast["tgl"]."</td>
                                                                    <td align=\"right\">".number_format($v_data_forecast["yt"], 2, ".", ",")."</td>
                                                                    <td align=\"right\">".number_format($v_data_forecast["lt"], 2, ".", ",")."</td>
                                                                    <td align=\"right\">".number_format($v_data_forecast["tt"], 2, ".", ",")."</td>
                                                                    <td align=\"right\">".number_format($v_data_forecast["yt_est"], 2, ".", ",")." Kg.</td>
                                                                    <td align=\"right\">".number_format($v_data_forecast["residual"], 2, ".", ",")."</td>");
                                                        print_r("</tr>");

                                                        // print_r($count_array);
                                                        $no++;
                                                    }

                                                    // print_r($str_header);
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="card card-outline-inverse">                
                            <!-------------------------------------------Data Table------------------------------------>
                            <div class="card-header">
                                <br><br>
                                <h4 class="m-b-0 text-white">Data Forecasting</h4>
                                <hr><br>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive m-t-40">
                                    <table id="example23" border="1" width="80%" style="border-collapse: collapse;">
                                        <thead>
                                            <tr>
                                                <th width="5%">No</th>
                                                <th width="20%">Periode</th>
                                                <th width="10%">Tgl. Pembelian</th>
                                                <th width="10%">Total Pembelian</th>
                                                <th width="10%">lt</th>
                                                <th width="15%">tt</th>
                                                <th width="15%">Analisa Peramalan</th>
                                                <th width="15%">Residual</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>No</th>
                                                <th>Periode</th>
                                                <th>Tgl. Pembelian</th>
                                                <th>Total Pembelian</th>
                                                <th>lt</th>
                                                <th>tt</th>
                                                <th>Analisa Peramalan</th>
                                                <th>Residual</th>
                                            </tr>
                                        </tfoot>
                                        <tbody id="output_laporan">
                                            <?php
                                                if($data_forecast){
                                                    $no = 1;
                                                    foreach ($data_forecast as $r_data_forecast => $v_data_forecast) {
                                                        // print_r("<pre>");
                                                        // print_r($v_data_forecast);
                                                         $str_periode = "minggu ke-".substr($v_data_forecast["periode"], 6, 1).", ".$month[(int)substr($v_data_forecast["periode"], 4, 2)]."/". substr($v_data_forecast["periode"], 0, 4);
                                                        print_r("<tr>
                                                                    <td align=\"center\">".$v_data_forecast["t"]."</td>
                                                                    <td align=\"right\">".$v_data_forecast["periode"]."</td>
                                                                    <td align=\"center\">".$v_data_forecast["tgl"]."</td>
                                                                    <td align=\"right\">".number_format($v_data_forecast["yt"], 2, ".", ",")."</td>
                                                                    <td align=\"right\">".number_format($v_data_forecast["lt"], 2, ".", ",")."</td>
                                                                    <td align=\"right\">".number_format($v_data_forecast["tt"], 2, ".", ",")."</td>
                                                                    <td align=\"right\">".number_format($v_data_forecast["yt_est"], 2, ".", ",")." Kg.</td>
                                                                    <td align=\"right\">".number_format($v_data_forecast["residual"], 2, ".", ",")."</td>");
                                                        print_r("</tr>");

                                                        // print_r($count_array);
                                                        $no++;
                                                    }

                                                    // print_r($str_header);
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- Row -->
            </div>

            