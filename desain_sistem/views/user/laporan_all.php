<!DOCTYPE html>
<html>
<head>
	<title>Laporan Akhir</title>
</head>
<body>
	<table border="1" width="95%" align="center" style="border-collapse: collapse;">
		<thead>
			<tr>
				<th rowspan="2">Tanggal</th>
				<th rowspan="2">Sales Net</th>
				<th rowspan="2">Struk</th>
				<th rowspan="2">AKM Sales</th>
				<th rowspan="2">AKM Struk</th>
				<th rowspan="2">SPD</th>
				<th rowspan="2">STD</th>
				<th rowspan="2">APC</th>
				<th colspan="3">GROWTH</th>
				
				<th rowspan="2">Penggantian</th>
				<th rowspan="2">Variance</th>
			</tr>
			<tr>
				<th>SPD</th>
				<th>STD</th>
				<th>APC</th>
			</tr>
		</thead>

		<tbody>
			<tr>
				<td colspan="13"><hr></td>
			</tr>

			<?php
				if(!empty($penjualan[0])){
					$no_tgl = 1;
					$t_sales_net = 0;
					$t_struk = 0;
					
					$t_sales_net_lalu = 0;
					$t_struk_lalu = 0;

					// print_r("<pre>");
					// 		print_r($penjualan[1]);
					
					foreach ($penjualan[0] as $r_penjualan => $v_penjualan) {
						// print_r($v_penjualan);

						$t_sales_net = $t_sales_net+$v_penjualan->sales_net;
						$t_struk = $t_struk+$v_penjualan->struk;

						$spd = 0;
						if($t_sales_net != 0){
							$spd = $t_sales_net/ $no_tgl;
						}

						$std = 0;
						if ($t_struk != 0) {
							$std = $t_struk/ $no_tgl;
						}
						
						$apc = 0;
						if($spd != 0 and $std != 0){
							$apc = $spd/$std;
						}

						#-----------GROWTH
						if(!empty($penjualan[1][$r_penjualan])){


							$t_sales_net_lalu = $t_sales_net_lalu+$penjualan[1][$r_penjualan]->sales_net;
							$t_struk_lalu = $t_struk_lalu+$penjualan[1][$r_penjualan]->struk;

							$spd_lalu = 0;
							if($t_sales_net_lalu != 0){
								$spd_lalu = $t_sales_net_lalu/ $no_tgl;

								$spd_gr = ($spd - $spd_lalu) / $spd_lalu * 100;
							}

							$std_lalu = 0;
							if ($t_struk_lalu != 0) {
								$std_lalu = $t_struk_lalu/ $no_tgl;

								$std_gr = ($std - $std_lalu) / $std_lalu * 100;
							}
							
							$apc_lalu = 0;
							if($spd_lalu != 0 and $std != 0){
								$apc_lalu = $spd_lalu/$std_lalu;

								$apc_gr = ($apc - $apc_lalu) / $apc_lalu * 100;
							}

							
						}else {
							$spd_gr = 0;
							$std_gr = 0;
							$apc_gr = 0;
						}
						


						


						
						echo "<tr>
								<td align=\"center\">".$no_tgl."</td>
								<td align=\"right\">".number_format($v_penjualan->sales_net, 2, '.', ',')."</td>
								<td align=\"right\">".$v_penjualan->struk."</td>
								<td align=\"right\">".number_format($t_sales_net, 2, '.', ',')."</td>
								<td align=\"right\">".number_format($t_struk, 2, '.', ',')."</td>
								<td align=\"right\">".number_format($spd, 2, '.', ',')."</td>
								<td align=\"right\">".number_format($std, 2, '.', ',')."</td>
								<td align=\"right\">".number_format($apc, 2, '.', ',')."</td>

								<td align=\"right\">".number_format($spd_gr, 2, '.', ',')."</td>
								<td align=\"right\">".number_format($std_gr, 2, '.', ',')."</td>
								<td align=\"right\">".number_format($apc_gr, 2, '.', ',')."</td>

								<td align=\"right\">".number_format($v_penjualan->pergantian, 2, '.', ',')."</td>
								<td align=\"right\">".number_format($v_penjualan->variance, 2, '.', ',')."</td>
							</tr>";
						$no_tgl++;
					}
				}
			?>
			
		</tbody>
	</table>

	<br>

	<table border="1" width="95%" align="center" style="border-collapse: collapse;">
		<thead>
			<tr>
				<th rowspan="2">Tanggal</th>
				<th rowspan="2">Discount (Rp. )</th>
				<th rowspan="2">AKM</th>
				<th rowspan="2">Rata-Rata</th>
				<th rowspan="2">L3M</th>
				<th rowspan="2">DSI Kemarin</th>

				<th colspan="2">NBR</th>
			</tr>
			<tr>
				
				<th>DSI SEKARANG</th>
				<th>BUAH</th>
			</tr>
		</thead>

		<!-- <tfoot>
			<tr>
				<th rowspan="2">Tanggal</th>
				<th rowspan="2">Discount (Rp. )</th>
				<th rowspan="2">AKM</th>
				<th rowspan="2">Rata-Rata</th>
				<th rowspan="2">L3M</th>
				<th rowspan="2">DSI Kemarin</th>

				<th colspan="2">NBR</th>
			</tr>
			<tr>
				
				<th>DSI SEKARANG</th>
				<th>BUAH</th>
			</tr>
		</tfoot> -->

		<tbody>
			<tr>
				<td colspan="8"><hr></td>
			</tr>

			<?php
				if(!empty($penjualan[0])){
					$no_tgl = 1;
					$akm_disc = 0;
					$dsi_kemarin = 0;

					$t_sales_net1 = 0;

					//bulan now-1
					$t_sales_net2 = 0;

					//bulan now-2
					$t_sales_net3 = 0;

					foreach ($penjualan[0] as $r_penjualan => $v_penjualan) {
						$disc = $v_penjualan->discount;
						$sales_net = $v_penjualan->sales_net;

						$akm_disc += $disc;
						$rata = $akm_disc/$no_tgl;

						$dsi_ini = $v_penjualan->dsi_ini;

						$margin = 0;
						if($dsi_ini != 0){
							$margin = $dsi_ini - $dsi_kemarin - $sales_net;	
						}

						$l3m = 0;
						$t_sales_net1 = $t_sales_net1+$v_penjualan->sales_net;
						$spd1 = $t_sales_net1/ $no_tgl;

						$spd2 = 0;
						if(!empty($penjualan[1])){
							if(isset($penjualan[1][$r_penjualan])){
								$t_sales_net2 = $t_sales_net2+$penjualan[1][$r_penjualan]->sales_net;
							}							
						}
						$spd2 = $t_sales_net2/ $no_tgl;


						$spd3 = 0;
						if(!empty($penjualan[2])){
							if(isset($penjualan[2][$r_penjualan])){
								$t_sales_net3 = $t_sales_net3+$penjualan[2][$r_penjualan]->sales_net;
							}
						}
						$spd3 = $t_sales_net3/ $no_tgl;

						$l3m = ($spd1 + $spd2 + $spd3) / 3;


						echo "<tr>
								<td align=\"center\">".$no_tgl."</td>
								<td align=\"right\">".number_format($disc, 2, '.', ',')."</td>
								<td align=\"right\">".number_format($akm_disc, 2, '.', ',')."</td>
								<td align=\"right\">".number_format($rata, 2, '.', ',')."</td>
								<td align=\"right\">".number_format($l3m, 2, '.', ',')."</td>
								<td align=\"right\">".number_format($dsi_kemarin, 2, '.', ',')."</td>

								<td align=\"right\">".number_format($dsi_ini, 2, '.', ',')."</td>
								<td align=\"right\">".number_format($margin, 2, '.', ',')."</td>
							</tr>";
						$no_tgl++;
						$dsi_kemarin = $dsi_ini;
					}
						
				}
			?>

			
		</tbody>
	</table>

	<script type="text/javascript">
		window.print();
	</script>
</body>
</html>