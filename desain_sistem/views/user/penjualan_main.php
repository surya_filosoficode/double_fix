            <?php
                // print_r("<pre>");
                // print_r($penjualan);
            ?>

                <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Data Pembelian Bahan Baku</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    
                </div>
                <div>
                    <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">            
                <br>
                <!-- Row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-outline-success">
                            <div class="card-header">
                                <h4 class="m-b-0 text-white">Input Data Laporan Pembelian Bahan Baku</h4>
                            </div>
                            <div class="card-body">
                                <form action="<?= base_url()."user/mainuser/insert_penjualan"?>" method="post" class="form-horizontal">
                                    <div class="form-body">
                                        <hr class="m-t-0 m-b-40">
                                        <div class="row">
                                            
                                            <div class="col-md-12">
                                            <?php
                                                // print_r("<pre>");
                                                // print_r($_SESSION);

                                                if(isset($_SESSION["response_laporan"])){
                                                    if(isset($_SESSION["response_laporan"]["msg_main"])){
                                                        if($_SESSION["response_laporan"]["msg_main"]["status"]){
                                                            print_r("
                                                                <div class=\"alert alert-info alert-rounded\">".$_SESSION["response_laporan"]["msg_main"]["msg"]."
                                                                    <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"> <span aria-hidden=\"true\">×</span> </button>
                                                                </div>");
                                                        }else{
                                                             print_r("
                                                                <div class=\"alert alert-danger alert-rounded\">".$_SESSION["response_laporan"]["msg_main"]["msg"]."<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"> <span aria-hidden=\"true\">×</span> </button>
                                                                </div>");
                                                        }
                                                    }
                                                }
                                            ?>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Tangal Pembelian Bahan Baku</label>
                                                    <div class="col-md-9">
                                                        <input type="date" class="form-control" id="tgl" name="tgl" value="<?php echo date("Y-m-d");?>" required="">
                                                        <p id="ex_tgl"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Periode (Minggu Ke-)</label>
                                                    <div class="col-md-9">
                                                        <!-- <input type="date" class="form-control" id="tgl" name="tgl" value="<?php echo date("Y-m-d");?>" required=""> -->
                                                        <select class="form-control" id="mgg_ke" name="mgg_ke">
                                                            <option value="1">Minggu Ke- 1</option>
                                                            <option value="2">Minggu Ke- 2</option>
                                                            <option value="3">Minggu Ke- 3</option>
                                                            <option value="4">Minggu Ke- 4</option>
                                                        </select>
                                                        <p id="ex_mgg_ke"></p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Periode (Bulan)</label>
                                                    <div class="col-md-9">
                                                        <select class="form-control" id="periode_bln" name="periode_bln">
                                                            <option value="01">Januari</option>
                                                            <option value="02">Februari</option>
                                                            <option value="03">Maret</option>
                                                            <option value="04">April</option>
                                                            <option value="05">Mei</option>
                                                            <option value="06">Juni</option>
                                                            <option value="07">Juli</option>
                                                            <option value="08">Agustus</option>
                                                            <option value="09">Septermber</option>
                                                            <option value="10">Oktober</option>
                                                            <option value="11">November</option>
                                                            <option value="12">Desember</option>
                                                        </select>
                                                        <p id="ex_periode_bln"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Periode (Tahun)</label>
                                                    <div class="col-md-9">
                                                        <input type="tetx" class="form-control" id="periode_th" name="periode_th" value="<?php echo date("Y");?>" required="">
                                                        <p id="ex_periode_th"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Data Pembeian Bahan Baku</label>
                                                    <div class="col-md-9">
                                                        <input type="number" class="form-control" placeholder="300000" id="penjualan" name="penjualan" required="">
                                                        <p id="ex_penjualan"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                    </div>    
                                    <hr>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" class="btn btn-success">Submit</button>
                                                        <button type="button" class="btn btn-danger">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6"> </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="card card-outline-inverse">
                            <div class="card-header">
                                <h4 class="m-b-0 text-white">Data Pembelian Bahan Baku</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive m-t-40">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th width="10%">No. </th>
                                                <th width="30%">Tanggal Pembelian Bahan Baku</th>
                                                <th width="20%">Periode Penggunaan Bahan Baku</th>
                                                <th width="20%">Total Pembelian</th>
                                                <th width="20%">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>No. </th>
                                                <th>Tanggal Pembelian Bahan Baku</th>
                                                <th>Periode Penggunaan Bahan Baku</th>
                                                <th>Total Pembelian</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            <?php
                                                if(!empty($penjualan)){
                                                    foreach ($penjualan as $r_penjualan => $v_penjualan){
                                                        $str_periode = "minggu ke-".substr($v_penjualan->periode, 6, 1).", ".$month[(int)substr($v_penjualan->periode, 4, 2)]."/". substr($v_penjualan->periode, 0, 4);
                                                        echo "<tr>
                                                                <td>".($r_penjualan+1)."</td>
                                                                <td>".$v_penjualan->tgl."</td>
                                                                <td>".$str_periode."</td>
                                                                <td align=\"right\">".number_format($v_penjualan->penjualan, 2,'.', ',')." Kg.</td>
                                                                
                                                                <td align=\"center\">
                                                                    <a onclick=\"up_penjualan('".$v_penjualan->id_lap."')\"><i class=\"btn btn-success\"><i class=\"fa fa-pencil-square-o\"></i></i></a>
                                                                    <a onclick=\"del_penjualan('".$v_penjualan->id_lap."')\"><i class=\"btn btn-danger\"><i class=\"fa fa-trash-o\"></i></i></a>
                                                                </td>
                                                            </tr>";
                                                    }
                                                }
                                            ?>

                                        </tbody>
                                    </table><!-- <a href="" ></a> -->
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- Row -->
            </div>

            <!-- sample modal content -->
                                <div class="modal fade bs-example-modal-lg" id="update_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myLargeModalLabel">UPDATE DATA PELAPORAN</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <form action="<?= base_url()."user/mainuser/up_penjualan"?>" method="post" class="form-horizontal">
                                            <div class="modal-body">
                                                <div class="form-body">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label class="control-label text-right col-md-3">Tangal Input</label>
                                                                <div class="col-md-9">
                                                                    <input type="date" class="form-control" id="up_tgl" name="tgl" value="<?php echo date("Y-m-d");?>" required="">
                                                                    <p id="ex_tgl"></p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label class="control-label text-right col-md-3">Periode (Minggu Ke-)</label>
                                                                <div class="col-md-9">
                                                                    <!-- <input type="date" class="form-control" id="tgl" name="tgl" value="<?php echo date("Y-m-d");?>" required=""> -->
                                                                    <select class="form-control" id="up_mgg_ke" name="mgg_ke">
                                                                        <option value="1">Minggu Ke- 1</option>
                                                                        <option value="2">Minggu Ke- 2</option>
                                                                        <option value="3">Minggu Ke- 3</option>
                                                                        <option value="4">Minggu Ke- 4</option>
                                                                    </select>
                                                                    <p id="ex_mgg_ke"></p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label class="control-label text-right col-md-3">Periode (Bulan)</label>
                                                                <div class="col-md-9">
                                                                    <select class="form-control" id="up_periode_bln" name="periode_bln">
                                                                        <option value="01">Januari</option>
                                                                        <option value="02">Februari</option>
                                                                        <option value="03">Maret</option>
                                                                        <option value="04">April</option>
                                                                        <option value="05">Mei</option>
                                                                        <option value="06">Juni</option>
                                                                        <option value="07">Juli</option>
                                                                        <option value="08">Agustus</option>
                                                                        <option value="09">Septermber</option>
                                                                        <option value="10">Oktober</option>
                                                                        <option value="11">November</option>
                                                                        <option value="12">Desember</option>
                                                                    </select>
                                                                    <p id="ex_periode_bln"></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label class="control-label text-right col-md-3">Periode (Tahun)</label>
                                                                <div class="col-md-9">
                                                                    <input type="tetx" class="form-control" id="up_periode_th" name="periode_th" value="<?php echo date("Y");?>" required="">
                                                                    <p id="ex_periode_th"></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label class="control-label text-right col-md-3">Data Pembeian</label>
                                                                <div class="col-md-9">
                                                                    <input type="number" class="form-control" placeholder="300000" id="up_penjualan" name="penjualan" required="">
                                                                    <p id="ex_up_penjualan"></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>
                                                    <!--/row-->

                                                    <input hidden="" type="number" class="form-control" placeholder="300000" id="up_id_lap" name="id_lap" required="">
                                                </div>    
                                                                                               
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-danger waves-effect text-left">Sipman</button>
                                            </div>
                                            </form>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->
                                
            <!--Morris JavaScript -->
            <script src="<?= base_url();?>template_admin/assets/plugins/raphael/raphael-min.js"></script>
            <script src="<?= base_url();?>template_admin/assets/plugins/morrisjs/morris.js"></script>


            <script type="text/javascript">
                var graph_data = [];
                <?php 
                    if (isset($graph_data)){
                        echo "graph_data = JSON.parse('".$graph_data."');";
                    } 
                ?>
                
                $(document).ready(function(){
                    // console.log(graph_data);


                    set_chart(graph_data);

                });

                function set_chart(val_data){
                    $(function () {
                    "use strict";
                    // Extra chart
                     Morris.Area({
                            element: 'extra-area-chart',
                            data: val_data,
                                    lineColors: ['#009efb'],
                                    xkey: 'period',
                                    ykeys: ['val_data'],
                                    labels: ['Sales Net'],
                                    pointSize: 0,
                                    lineWidth: 0,
                                    resize:false,
                                    fillOpacity: 0.8,
                                    behaveLikeLine: true,
                                    gridLineColor: '#e0e0e0',
                                    hideHover: 'auto'
                            
                        });
                    }); 
                }
                
                function currency(x){
                    return x.toLocaleString('us-EG');
                }

                function up_penjualan(id_lap){
                    clear_up();
                    var data_main =  new FormData();
                    data_main.append('id_lap', id_lap);    
                    $.ajax({
                        url: "<?php echo base_url()."/user/mainuser/index_up_penjualan/";?>", // point to server-side PHP script 
                        dataType: 'html',  // what to expect back from the PHP script, if anything
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: data_main,                         
                        type: 'post',
                        success: function(res){
                            // console.log(res);
                            res_update(res);
                            // $("#out_up_mhs").html(res);
                        }
                    });
                    $("#update_modal").modal('show');
                }

                function res_update(res){
                    var data = JSON.parse(res);
                    console.log(data);

                    if(data.status){
                        var main_data = data.val;
                        $("#up_tgl").val(main_data.tgl);
                        $("#up_penjualan").val(main_data.penjualan);

                        var mgg = main_data.periode.substr(6,1);
                        var periode_bln = main_data.periode.substr(4,2);
                        var periode_th = main_data.periode.substr(0,4);

                        // console.log(main_data.periode);

                        $("#up_mgg_ke").val(mgg);
                        $("#up_periode_bln").val(periode_bln);
                        $("#up_periode_th").val(periode_th);
                        
                        $("#up_id_lap").val(main_data.id_lap);

                        // console.log(main_data);
                        

                        //------------------------------------------------------------------update sales net
                        var masbro = 0;
                        if($("#up_penjualan").val() != null){
                            masbro = currency(parseInt($("#up_penjualan").val())) + " Kg.";    
                            // console.log($("#penjualan";
                        }else{
                            masbro = 0;
                        }
                        $("#ex_up_penjualan").html(masbro);

                    }else{
                        clear_up();
                    }
                }

                function clear_up(){
                        $("#up_tgl").val("");
                        $("#up_penjualan").val("");

                        $("#up_id_lap").val("");
                }

                function del_penjualan(id_penjualan){
                    // $("#update_modal").modal('show');
                    var conf = confirm("Apakah anda yakin untuk menghapus "+id_penjualan+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_penjualan+" akan terhapus semau.. !!!!!!! ");

                    if(conf){
                        window.location.href = "<?= base_url()."user/mainuser/delete_penjualan/";?>"+id_penjualan;
                    }else{

                    }
                }

                // $("#tgl").keyup(function(){
                //     $("#ex_tgl").html();
                // });

                $("#penjualan").keyup(function(){
                    var masbro = 0;
                    if($("#penjualan").val() != null){
                        masbro = currency(parseInt($("#penjualan").val())) + " Kg.";    
                        // console.log($("#penjualan";
                    }else{
                        masbro = 0;
                    }
                    $("#ex_penjualan").html(masbro);
                });

                // $("#struk").keyup(function(){

                //     $("#ex_struk").html();
                // });

                $("#penggantian").keyup(function(){
                    var masbro = 0;
                    if($("#penggantian").val() != null){
                        masbro = currency(parseInt($("#penggantian").val())) + " Kg.";    
                    }else{
                        masbro = 0;
                    }
                    
                    $("#ex_penggantian").html(masbro);
                });

                $("#variance").keyup(function(){
                    var masbro = 0;
                    if($("#variance").val() != null){
                        masbro =currency(parseInt($("#variance").val())) + " Kg.";    
                    }else{
                        masbro = 0;
                    }
                    
                    $("#ex_variance").html(masbro);
                });

                $("#disc").keyup(function(){
                    var masbro = 0;
                    if($("#disc").val() != null){
                        masbro = currency(parseInt($("#disc").val()))+ " Kg.";    
                    }else{
                        masbro = 0;
                    }
                    
                    $("#ex_disc").html(masbro);
                    // $("#ex_disc").html();
                });

                $("#dsi_ini").keyup(function(){
                    var masbro = 0;
                    if($("#dsi_ini").val() != null){
                        masbro = currency(parseInt($("#dsi_ini").val()))+ " Kg.";    
                    }else{
                        masbro = 0;
                    }
                    
                    $("#ex_dsi").html(masbro);
                    // $("#ex_dsi").html();
                });


                // --------------------------------------------------------------------update------------------------------------------------------------

                $("#up_penjualan").keyup(function(){
                    var masbro = 0;
                    if($("#up_penjualan").val() != null){
                        masbro = currency(parseInt($("#up_penjualan").val()))+ " Kg.";    
                        // console.log($("#penjualan";
                    }else{
                        masbro = 0;
                    }
                    $("#ex_up_penjualan").html(masbro);
                });

                // $("#struk").keyup(function(){

                //     $("#ex_struk").html();
                // });

                $("#up_penggantian").keyup(function(){
                    var masbro = 0;
                    if($("#up_penggantian").val() != null){
                        masbro = currency(parseInt($("#up_penggantian").val()))+ " Kg.";    
                    }else{
                        masbro = 0;
                    }
                    
                    $("#ex_up_penggantian").html(masbro);
                });

                $("#up_variance").keyup(function(){
                    var masbro = 0;
                    if($("#up_variance").val() != null){
                        masbro = currency(parseInt($("#up_variance").val()))+ " Kg.";    
                    }else{
                        masbro = 0;
                    }
                    
                    $("#ex_up_variance").html(masbro);
                });

                $("#up_disc").keyup(function(){
                    var masbro = 0;
                    if($("#up_disc").val() != null){
                        masbro = currency(parseInt($("#up_disc").val()))+ " Kg.";    
                    }else{
                        masbro = 0;
                    }
                    
                    $("#ex_up_disc").html(masbro);
                    // $("#ex_disc").html();
                });

                $("#up_dsi_ini").keyup(function(){
                    var masbro = 0;
                    if($("#up_dsi_ini").val() != null){
                        masbro = currency(parseInt($("#up_dsi_ini").val())) + " Kg.";    
                    }else{
                        masbro = 0;
                    }
                    
                    $("#ex_up_dsi").html(masbro);
                    // $("#ex_dsi").html();
                });

                
            </script>